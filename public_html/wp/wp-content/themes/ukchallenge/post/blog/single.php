<div class="p-blog-box single">
  <div class="p-container">
    <main role="main">
      <div class="meta">
        <p class="day"><?php the_time('Y.m.d'); ?></p>
        <?php
        $terms = get_the_terms($post -> ID, 'blog_cat');
        foreach($terms as $term): ?>
        <?php
        $cat_name = $term->name;
        ?>
        <?php endforeach; ?>
        <p class="cat"><?php echo $cat_name; ?></p>
      </div>
      <h1 class="title"><?php the_title();?></h1>
      <div class="eyecatch">
        <?php if (has_post_thumbnail()): ?>
        <?php echo get_the_post_thumbnail($post->ID, 'full'); ?>
        <?php else: ?>
        <?php endif; ?>
      </div>
      <div class="editor">
        <?php if(have_posts()):while(have_posts()):the_post(); ?>
        <?php the_content(); ?>
        <?php endwhile; endif; ?>
      </div>
      <div class="button">
        <a href="<?php echo home_url(); ?>/blog/" class="m-button01 small back"><span>ブログ一覧へ戻る</span></a>
      </div>
    </main>
    <?php get_sidebar(); ?>
  </div>
</div>