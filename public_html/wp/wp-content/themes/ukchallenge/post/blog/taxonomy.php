<?php
$tax_slug = get_query_var('taxonomy');                  // 現在のページのタクソノミースラッグ
$term_slug = get_query_var('term');                     // 現在のページのタームスラッグ
$term_meta = get_term_by("slug",$term_slug,$tax_slug);  // 現在のページのターム情報を取得
$term_id = $term_meta->term_id;                         // 現在のページのタームID
$term_name = $term_meta->name;                          // 現在のページのターム名
?>
<div class="p-blog-box archive">
  <div class="p-container">
    <main role="main">
      <ul class="list">
        <?php
        $paged = get_query_var('paged')? get_query_var('paged') : 1;
        $args = array(
          'post_type' => 'blog',
          'posts_per_page' => 5,
          'paged' => $paged,
          'post_status' => 'publish',
          'tax_query' => array(
            'relation' => 'OR',
            array(
              'taxonomy' => $tax_slug,
              'terms' => $term_slug,
              'field' => 'slug',
              'operator' => 'IN'
            ),
          ),
        );
        ?>
        <?php $wp_query = new WP_Query( $args ); ?>
        <?php if( $wp_query->have_posts() ) : ?>
        <?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
        <li>
          <a href="<?php the_permalink(); ?>">
            <div class="img">
              <?php if (has_post_thumbnail()): ?>
              <?php echo get_the_post_thumbnail($post->ID, 'full'); ?>
              <?php else: ?>
              <?php endif; ?>
              <?php
              $terms = get_the_terms($post -> ID, 'blog_cat');
              foreach($terms as $term): ?>
              <?php
              $cat_name = $term->name;
              ?>
              <?php endforeach; ?>
              <p class="cat"><?php echo $cat_name; ?></p>
            </div>
            <div class="text">
              <p class="day"><?php the_time('Y.m.d'); ?></p>
              <h2 class="title">
                <?php
                $text_cnt = 22;
                $blog_title = strip_tags(get_the_title());
                if(mb_strlen($blog_title, 'UTF-8')>$text_cnt){
                  $blog_title_mb = mb_substr($blog_title, 0, $text_cnt, 'UTF-8');
                  echo $blog_title_mb.'...';
                }else{
                  echo $blog_title;
                }
                ?>
              </h2>
              <p class="read">
                <?php
                $text_cnt = 62;
                $blog_content = strip_tags(get_the_content());
                if(mb_strlen($blog_content, 'UTF-8')>$text_cnt){
                  $blog_content_mb = mb_substr($blog_content, 0, $text_cnt, 'UTF-8');
                  echo $blog_content_mb.'...';
                }else{
                  echo $blog_content;
                }
                ?>
              </p>
            </div>
          </a>
        </li>
        <?php endwhile; ?>
        <?php else: ?>
        <?php endif; ?>
      </ul>
      <?php m_page_navigation(); ?>
      <?php wp_reset_query(); ?>
    </main>
    <?php get_sidebar(); ?>
  </div>
</div>