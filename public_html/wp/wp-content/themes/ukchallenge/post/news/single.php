<div class="p-news-box single">
  <div class="p-container">
    <div class="meta">
      <p class="day"><?php the_time('Y.m.d'); ?></p>
    </div>
    <h1 class="title"><?php the_title();?></h1>
    <div class="eyecatch">
      <?php if (has_post_thumbnail()): ?>
      <?php echo get_the_post_thumbnail($post->ID, 'full'); ?>
      <?php else: ?>
      <?php endif; ?>
    </div>
    <div class="editor">
      <?php if(have_posts()):while(have_posts()):the_post(); ?>
      <?php the_content(); ?>
      <?php endwhile; endif; ?>
    </div>
    <div class="button">
      <a href="<?php echo home_url(); ?>/news/" class="m-button01 small back"><span>お知らせ一覧へ戻る</span></a>
    </div>
  </div>
</div>