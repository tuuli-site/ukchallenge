<div class="p-news-box archive">
  <div class="p-container">
    <ul>
      <?php
      $paged = get_query_var('paged')? get_query_var('paged') : 1;
      $args = array(
        'post_type' => 'news',
        'posts_per_page' => 10,
        'paged' => $paged,
        'post_status' => 'publish'
      );
      ?>
      <?php $wp_query = new WP_Query( $args ); ?>
      <?php if( $wp_query->have_posts() ) : ?>
      <?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
      <li>
        <a href="<?php the_permalink(); ?>" class="flex">
          <div class="day">
            <p><?php the_time('Y.m.d'); ?></p>
          </div>
          <div class="title">
            <h3><?php the_title(); ?></h3>
          </div>
        </a>
      </li>
      <?php endwhile; ?>
      <?php else: ?>
      <?php endif; ?>
    </ul>
    <?php m_page_navigation(); ?>
    <?php wp_reset_query(); ?>
  </div>
</div>