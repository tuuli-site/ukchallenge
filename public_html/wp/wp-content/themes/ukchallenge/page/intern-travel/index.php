<div class="m-nav-box">
  <div class="container">
    <ul class="flex">
      <li>
        <a href="#about">
          <span>About</span>
          <p>企業研修・旅行</p>
        </a>
      </li>
      <li>
        <a href="#cost">
          <span>Cost</span>
          <p>費用</p>
        </a>
      </li>
      <li>
        <a href="#schedule">
          <span>Schedule</span>
          <p>スケジュール例</p>
        </a>
      </li>
      <li>
        <?php if(have_rows('cf_page_voiceRepeat')): ?>
        <a href="#voice">
          <span>Voice</span>
          <p>参加者の声</p>
        </a>
        <?php else: ?>
        <a href="#support">
          <span>Support</span>
          <p>充実したサポート</p>
        </a>
        <?php endif; ?>
      </li>
    </ul>
  </div>
</div>
<div class="m-point-box" id="about">
  <div class="container">
    <div class="box">
      <div class="img">
        <div class="border">
          <img src="<?php echo get_theme_file_uri(); ?>/assets/images/page/intern-travel/img_01.jpg" alt="">
        </div>
      </div>
      <div class="text">
        <div class="heading">
          <div class="point">
            <p><span>POINT</span><strong>01</strong></p>
          </div>
          <p class="title">社員のモチベーションも上がる<br>海外研修</p>
        </div>
        <p class="read">各社に合わせた企業向け語学研修をアレンジします。各業界の専門用語を中心に語学を学べるプログラムもアレンジ可能です。また、現地企業のスタッフによる講義や各業界の現地サービス見学など柔軟にご相談いただけます。</p>
      </div>
    </div>
    <div class="box">
      <div class="img">
        <div class="border">
          <img src="<?php echo get_theme_file_uri(); ?>/assets/images/page/intern-travel/img_02.jpg" alt="">
        </div>
      </div>
      <div class="text">
        <div class="heading">
          <div class="point">
            <p><span>POINT</span><strong>02</strong></p>
          </div>
          <h2 class="title">予算や希望に柔軟に対応する<br>イギリス旅行プラン</h2>
        </div>
        <p class="read">研修ではなくインセンティブ旅行や学生の卒業旅行のアレンジも受付けています。観光やスポーツ観戦などを組み込み充実した研修・旅行をお過ごしいただけます。サッカーの試合や観光地のチケットの手配も可能です。アレンジ力の柔軟さが最大の魅力になります。</p>
      </div>
    </div>
  </div>
</div>
<div class="m-cost-box" id="cost">
  <div class="container">
    <div class="box">
      <div class="wrap">
        <div class="m-title04">
          <p class="en">Season &amp; Cost</p>
          <p class="title">研修費・旅行費</p>
        </div>
        <p class="read">内容、時期などにより変わる為まずはお気軽にお問い合わせください。<br><span><strong>ご希望の予算からプランを組み立てる</strong></span>ことも可能です。</p>
        <div class="button">
          <a href="<?php echo home_url(); ?>/contact/" class="m-button01 big black">メールでお問い合わせはこちら</a>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="m-schedule-box" id="schedule">
  <div class="container">
    <div class="m-title04">
      <p class="en">Schedule</p>
      <p class="title">スケジュール例</p>
    </div>
    <div class="mini-table">
      <table>
        <tr>
          <th colspan="2">内容</th>
        </tr>
        <tr>
          <th>Day 1</th>
          <td>ロンドン到着、ロンドン観光</td>
        </tr>
        <tr>
          <th>Day 2</th>
          <td>（午前）語学研修<br>
          （午後）現地サロンスタッフ講義</td>
        </tr>
        <tr>
          <th>Day 3</th>
          <td>（午前）語学研修<br>
            （午後）現地サロン見学</td>
        </tr>
        <tr>
          <th>Day 4</th>
          <td>（午前）語学研修<br>
            （午後）技術研修</td>
        </tr>
        <tr>
          <th>Day 5</th>
          <td>観光</td>
        </tr>
        <tr>
          <th>Day 6</th>
          <td>ロンドン出発</td>
        </tr>
      </table>
    </div>
  </div>
</div>
<div class="m-photo-box">
  <img src="<?php echo get_theme_file_uri(); ?>/assets/images/common/common_photobox_01.jpg" alt="">
  <img src="<?php echo get_theme_file_uri(); ?>/assets/images/common/common_photobox_02.jpg" alt="">
  <img src="<?php echo get_theme_file_uri(); ?>/assets/images/common/common_photobox_03.jpg" alt="">
  <img src="<?php echo get_theme_file_uri(); ?>/assets/images/common/common_photobox_04.jpg" alt="">
  <img src="<?php echo get_theme_file_uri(); ?>/assets/images/common/common_photobox_05.jpg" alt="">
  <img src="<?php echo get_theme_file_uri(); ?>/assets/images/common/common_photobox_06.jpg" alt="">
  <img src="<?php echo get_theme_file_uri(); ?>/assets/images/common/common_photobox_07.jpg" alt="">
  <img src="<?php echo get_theme_file_uri(); ?>/assets/images/common/common_photobox_08.jpg" alt="">
  <img src="<?php echo get_theme_file_uri(); ?>/assets/images/common/common_photobox_09.jpg" alt="">
  <img src="<?php echo get_theme_file_uri(); ?>/assets/images/common/common_photobox_10.jpg" alt="">
  <img src="<?php echo get_theme_file_uri(); ?>/assets/images/common/common_photobox_11.jpg" alt="">
  <img src="<?php echo get_theme_file_uri(); ?>/assets/images/common/common_photobox_12.jpg" alt="">
</div>
<?php if(have_rows('cf_page_voiceRepeat')): ?>
<div class="m-voice-box" id="voice">
  <div class="container">
    <div class="m-title04">
      <p class="en">Voice</p>
      <p class="title">参加者の声</p>
    </div>
    <?php while(have_rows('cf_page_voiceRepeat')):the_row(); ?>
    <div class="box">
      <p class="name"><?php the_sub_field('cf_page_voiceRepeat_name'); ?>さん<?php if(get_sub_field('cf_page_voiceRepeat_kana')): ?>（<?php the_sub_field('cf_page_voiceRepeat_kana'); ?>）<?php endif; ?></p>
      <p class="heading"><?php the_sub_field('cf_page_voiceRepeat_title'); ?></p>
      <p class="read"><?php the_sub_field('cf_page_voiceRepeat_read'); ?></p>
      <?php if(have_rows('cf_page_voiceRepeat_photoRepeat')): ?>
      <div class="photo">
        <?php while(have_rows('cf_page_voiceRepeat_photoRepeat')):the_row(); ?>
        <img src="<?php the_sub_field('cf_page_voiceRepeat_photoRepeat_img'); ?>" alt="">
        <?php endwhile; ?>
      </div>
      <?php endif; ?>
      <?php if(get_sub_field('cf_page_voiceRepeat_remark')): ?>
      <p class="remark"><?php the_sub_field('cf_page_voiceRepeat_remark'); ?></p>
      <?php endif; ?>
    </div>
    <?php endwhile; ?>
  </div>
</div>
<?php endif; ?>