<div class="description">
  <div class="p-container">
    <div class="m-title03">
      <p class="en">Message</p>
      <h2 class="title">代表メッセージ</h2>
    </div>
    <div class="read">
      <p>UK CHALLENGEは私自身がイギリス留学から得たものを多くの方に共有したいという思いから始まりました。<br>私は大学卒業後就職しましたが留学が諦められず20代半ば退職しロンドンにサッカーのコーチング留学に行きました。<br>ロンドンでの挑戦と新たな経験の日々から得たものは日本では手にすることができないものばかりでした。</p>
      <p>私は特に10代、20代の若い世代に海外を経験することを勧めたいです。海外での経験はその後の将来に大きく影響してくると言い切れます。<br>ただし留学はただ行けば良いというものではなくどんな目的を持って留学するのかが一番大事です。留学のプランや予算を考える前になぜ留学したいのか、何を身につけたいのか、など一度考えてみてください。</p>
      <p>UK CHALLENGEでは留学の目的を設定するところから相談に乗り『無駄なく得るものが多い留学』を実現させることを目標にサービスを提供してまいります。</p>
    </div>
  </div>
</div>
<div class="about">
  <div class="p-container">
    <div class="m-title03">
      <p class="en">About</p>
      <h2 class="title">事業概要</h2>
    </div>
    <table>
      <tr>
        <th>事業名</th>
        <td>UK CHALLENGE</td>
      </tr>
      <tr>
        <th>サービス開始日</th>
        <td>2018年4月1日</td>
      </tr>
      <tr>
        <th>代表者名</th>
        <td>太田　翔</td>
      </tr>
      <tr>
        <th>E-mail</th>
        <td>info@ukchallenge.net</td>
      </tr>
      <tr>
        <th>事業内容</th>
        <td>イギリス留学エージェント業<br>イギリスへのスポーツ留学、語学留学、芸術留学、<br class="nosp">正規留学の手配<br>イギリスへのスポーツ遠征、企業研修・旅行の手配<br>イギリス旅行の手配<br>イギリス留学情報の発信<br>イギリスビザ取得サポート<br>留学後の就職相談</td>
      </tr>
    </table>
  </div>
</div>