<div class="m-nav-box">
  <div class="container">
    <ul class="flex">
      <li>
        <a href="#about">
          <span>About</span>
          <p>サッカービジネス留学とは</p>
        </a>
      </li>
      <li>
        <a href="#cost">
          <span>Cost</span>
          <p>留学費用</p>
        </a>
      </li>
      <li>
        <a href="#schedule">
          <span>Schedule</span>
          <p>スケジュール例</p>
        </a>
      </li>
      <li>
        <?php if(have_rows('cf_page_voiceRepeat')): ?>
        <a href="#voice">
          <span>Voice</span>
          <p>参加者の声</p>
        </a>
        <?php else: ?>
        <a href="#support">
          <span>Support</span>
          <p>充実したサポート</p>
        </a>
        <?php endif; ?>
      </li>
    </ul>
  </div>
</div>
<div class="m-point-box" id="about">
  <div class="container">
    <div class="box">
      <div class="img">
        <div class="border">
          <img src="<?php echo get_theme_file_uri(); ?>/assets/images/page/football/business/img_01.jpg" alt="">
        </div>
      </div>
      <div class="text">
        <div class="heading">
          <div class="point">
            <p><span>POINT</span><strong>01</strong></p>
          </div>
          <p class="title">現地クラブでクラブ運営を学ぶ</p>
        </div>
        <p class="read">現地のセミプロクラブで実際にクラブの一員として運営に携わり、サッカークラブがどのように動いているのか学んでいきます。留学をしながらキャリアを積むことができるので、将来サッカー業界で働きたいと考えている方にとって最適なプログラムです。</p>
      </div>
    </div>
    <div class="box">
      <div class="img">
        <div class="border">
          <img src="<?php echo get_theme_file_uri(); ?>/assets/images/page/football/business/img_02.jpg" alt="">
        </div>
      </div>
      <div class="text">
        <div class="heading">
          <div class="point">
            <p><span>POINT</span><strong>02</strong></p>
          </div>
          <h2 class="title">ビジネスレベルの英語力を目指す</h2>
        </div>
        <p class="read">英語力が高ければ高いほど携われる業務の幅が増えますが、英語が苦手な方でも参加可能です。基本的には午前中に語学学校に通うので英語力もビジネスレベルまで高めることを目標にして日々学んでいきます。</p>
      </div>
    </div>
  </div>
</div>
<div class="m-cost-box" id="cost">
  <div class="container">
    <div class="box">
      <div class="wrap">
        <div class="m-title04">
          <p class="en">Season &amp; Cost</p>
          <p class="title">留学費用</p>
        </div>
        <p class="read">内容、時期などにより変わる為まずはお気軽にお問い合わせください。<br><span><strong>ご希望の予算からプランを組み立てる</strong></span>ことも可能です。</p>
        <div class="button">
          <a href="<?php echo home_url(); ?>/contact/" class="m-button01 big black">メールでお問い合わせはこちら</a>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="m-photo-box">
  <img src="<?php echo get_theme_file_uri(); ?>/assets/images/common/common_photobox_01.jpg" alt="">
  <img src="<?php echo get_theme_file_uri(); ?>/assets/images/common/common_photobox_02.jpg" alt="">
  <img src="<?php echo get_theme_file_uri(); ?>/assets/images/common/common_photobox_03.jpg" alt="">
  <img src="<?php echo get_theme_file_uri(); ?>/assets/images/common/common_photobox_04.jpg" alt="">
  <img src="<?php echo get_theme_file_uri(); ?>/assets/images/common/common_photobox_05.jpg" alt="">
  <img src="<?php echo get_theme_file_uri(); ?>/assets/images/common/common_photobox_06.jpg" alt="">
  <img src="<?php echo get_theme_file_uri(); ?>/assets/images/common/common_photobox_07.jpg" alt="">
  <img src="<?php echo get_theme_file_uri(); ?>/assets/images/common/common_photobox_08.jpg" alt="">
  <img src="<?php echo get_theme_file_uri(); ?>/assets/images/common/common_photobox_09.jpg" alt="">
  <img src="<?php echo get_theme_file_uri(); ?>/assets/images/common/common_photobox_10.jpg" alt="">
  <img src="<?php echo get_theme_file_uri(); ?>/assets/images/common/common_photobox_11.jpg" alt="">
  <img src="<?php echo get_theme_file_uri(); ?>/assets/images/common/common_photobox_12.jpg" alt="">
</div>
<?php if(have_rows('cf_page_voiceRepeat')): ?>
<div class="m-voice-box" id="voice">
  <div class="container">
    <div class="m-title04">
      <p class="en">Voice</p>
      <p class="title">参加者の声</p>
    </div>
    <?php while(have_rows('cf_page_voiceRepeat')):the_row(); ?>
    <div class="box">
      <p class="name"><?php the_sub_field('cf_page_voiceRepeat_name'); ?>さん<?php if(get_sub_field('cf_page_voiceRepeat_kana')): ?>（<?php the_sub_field('cf_page_voiceRepeat_kana'); ?>）<?php endif; ?></p>
      <p class="heading"><?php the_sub_field('cf_page_voiceRepeat_title'); ?></p>
      <p class="read"><?php the_sub_field('cf_page_voiceRepeat_read'); ?></p>
      <?php if(have_rows('cf_page_voiceRepeat_photoRepeat')): ?>
      <div class="photo">
        <?php while(have_rows('cf_page_voiceRepeat_photoRepeat')):the_row(); ?>
        <img src="<?php the_sub_field('cf_page_voiceRepeat_photoRepeat_img'); ?>" alt="">
        <?php endwhile; ?>
      </div>
      <?php endif; ?>
      <?php if(get_sub_field('cf_page_voiceRepeat_remark')): ?>
      <p class="remark"><?php the_sub_field('cf_page_voiceRepeat_remark'); ?></p>
      <?php endif; ?>
    </div>
    <?php endwhile; ?>
  </div>
</div>
<?php endif; ?>