<div class="container">
  <p class="description">イングランドには世界最高峰のプレミアリーグがあり、4部までがプロリーグ、5～8部がセミプロリーグ、<br class="nosp">その下にあるアマチュアリーグを合わせると24部まであると言われています。<br>サッカーが生活の一部になっているこの国で、この地でしか吸収できないスキル、<br class="nosp">考え方、文化を学ぶことができるでしょう。</p>
  <div class="box">
    <h2 class="m-title02">選手留学</h2>
    <p>語学学校に通いながら現地クラブで選手としてプレーし生活を送ります。<br> 留学後にヨーロッパや日本でプロ選手を目指す方もいれば、<br class="nosp"> 「英語を勉強しながら高いレベルでプレーしたい」という方もいます。</p>
    <div class="button">
      <a href="<?php echo home_url(); ?>/football/player/" class="m-button01 small"><span>詳しくはこちら</span></a>
    </div>
  </div>
  <div class="box">
    <h2 class="m-title02">指導者留学</h2>
    <p>語学学校に通いながら現地クラブで研修を受け、資格取得を目指します。<br>コーチングと英語の能力次第では実際にアシスタントコーチとして選手を<br class="nosp">指導することも可能です。</p>
    <div class="button">
      <a href="<?php echo home_url(); ?>/football/leader/" class="m-button01 small"><span>詳しくはこちら</span></a>
    </div>
  </div>
  <div class="box">
    <h2 class="m-title02">トレーナー留学</h2>
    <p>語学学校に通いながら現地クラブで研修を受け、トレーナーとしての技術向上を目指します。トレーナーとしての技術と英語の能力次第では実際に選手の体に触ったり、リハビリメニューを作成することも可能です。</p>
    <div class="button">
      <a href="<?php echo home_url(); ?>/football/trainer/" class="m-button01 small"><span>詳しくはこちら</span></a>
    </div>
  </div>
  <div class="box">
    <h2 class="m-title02">サッカービジネス留学</h2>
    <p>現地のセミプロクラブで実際にクラブの一員として運営に携わり、サッカークラブがどのように動いているのか学んでいきます。留学をしながらキャリアを積むことができるので、<br class="nosp">将来サッカー業界で働きたいと考えている方にとって最適なプログラムです。</p>
    <div class="button">
      <a href="<?php echo home_url(); ?>/football/business/" class="m-button01 small"><span>詳しくはこちら</span></a>
    </div>
  </div>
</div>