<div class="m-nav-box">
  <div class="container">
    <ul class="flex">
      <li>
        <a href="#about">
          <span>About</span>
          <p>サッカー遠征とは</p>
        </a>
      </li>
      <li>
        <a href="#cost">
          <span>Cost</span>
          <p>費用</p>
        </a>
      </li>
      <li>
        <a href="#schedule">
          <span>Schedule</span>
          <p>スケジュール例</p>
        </a>
      </li>
      <li>
        <?php if(have_rows('cf_page_voiceRepeat')): ?>
        <a href="#voice">
          <span>Voice</span>
          <p>参加者の声</p>
        </a>
        <?php else: ?>
        <a href="#support">
          <span>Support</span>
          <p>充実したサポート</p>
        </a>
        <?php endif; ?>
      </li>
    </ul>
  </div>
</div>
<div class="m-point-box" id="about">
  <div class="container">
    <div class="box">
      <div class="img">
        <div class="border">
          <img src="<?php echo get_theme_file_uri(); ?>/assets/images/page/football-trip/img_01.jpg" alt="">
        </div>
      </div>
      <div class="text">
        <div class="heading">
          <div class="point">
            <p><span>POINT</span><strong>01</strong></p>
          </div>
          <p class="title">試合、練習、観戦など<br>自由にカスタマイズできる遠征</p>
        </div>
        <p class="read">プロ下部組織との練習試合やプロコーチによる練習のアレンジが可能です。その他にプレミアリーグ試合観戦、語学レッスン、指導者資格取得、スタジアムツアー、市内観光などご希望に合わせて様々な内容を組み合わせることができます。</p>
      </div>
    </div>
    <div class="box">
      <div class="img">
        <div class="border">
          <img src="<?php echo get_theme_file_uri(); ?>/assets/images/page/football-trip/img_02.jpg" alt="">
        </div>
      </div>
      <div class="text">
        <div class="heading">
          <div class="point">
            <p><span>POINT</span><strong>02</strong></p>
          </div>
          <h2 class="title">ヨーロッパの大会に挑戦も可能</h2>
        </div>
        <p class="read">また、通常のチーム遠征に加え、イギリス以外も含めたヨーロッパで開催される国際大会のご紹介も行なっています。強豪ローカルクラブが参加する大会からプロクラブ 下部組織のみが参加する大会など選択肢も様々ございます。</p>
      </div>
    </div>
  </div>
</div>
<div class="m-cost-example-box" id="cost">
  <div class="container">
    <div class="m-title04">
      <p class="en">Cost</p>
      <p class="title">遠征費用一例</p>
    </div>
    <div class="box">
      <div class="heading">
        <p class="title"><strong>30～50万円</strong>／ 選手</p>
        <p class="small">※選手約20名で算出<br>※スタッフ費用はクラブ負担</p>
      </div>
      <div class="wrap">
        <div class="blue">
          <p class="title">含まれるもの</p>
          <ul>
            <li>UEFA公認コーチ練習指導費（4回）</li>
            <li>マッチメイク代（3試合）</li>
            <li>審判代</li>
            <li>サッカー施設使用料</li>
            <li>食事代（1日3食）</li>
            <li>イギリス国内移動費</li>
            <li>プロ試合観戦費 1試合</li>
            <li>ウェンブリースタジアムツアー</li>
            <li>洗濯代</li>
            <li>全日程帯同サポート</li>
            <li>サッカー用具レンタル代<br class="nopc">（ボール、マーカー、ビブス）</li>
            <li>航空券代</li>
            <li>宿泊費（6泊7日）</li>
          </ul>
        </div>
        <div class="red">
          <p class="title">オプション</p>
          <ul>
            <li>プロチーム下部組織との練習試合<br class="nopc">（チームレベルによる）</li>
            <li>練習試合数を増やす</li>
            <li>現地チームと試合後の交流</li>
            <li>Arsenal FCのEmirates Stadium<br class="nopc">などのスタジアムツアー</li>
            <li>有料観光施設への入場</li>
            <li>パブやレストランでの外食</li>
            <li>プロコーチによる<br class="nopc">サッカー分析講義やその他講義</li>
            <li>英語レッスン</li>
            <li>UEFA公認コーチによる<br class="nopc">練習試合の指導</li>
          </ul>
        </div>
        <div class="gray">
          <p class="title">含まれないもの</p>
          <ul>
            <li>海外旅行保険代</li>
            <li>雑費（お小遣い等）</li>
            <li>携帯電話などの通信費</li>
            <li>日本国内移動費</li>
          </ul>
        </div>
      </div>
    </div>
    <div class="remark">
      <p>※航空券は時期により料金が異なります。</p>
      <p>※為替変動により遠征料金が大きく変動する可能性がございます。</p>
      <p>※その他ご要望にも柔軟にご対応致します</p>
    </div>
  </div>
</div>
<div class="m-schedule-box" id="schedule">
  <div class="container">
    <div class="m-title04">
      <p class="en">Schedule</p>
      <p class="title">スケジュール例</p>
    </div>
    <div class="mini-table">
      <table>
        <tr>
          <th colspan="2">内容</th>
        </tr>
        <tr>
          <th>Day 1</th>
          <td>東京発<br>
            ロンドンヒースロー空港着<br>
            ヒースロー空港 ⇒ 宿舎<br>
            チェックイン<br>
            夕食</td>
        </tr>
        <tr>
          <th>Day 2</th>
          <td>UEFA公認コーチによる指導①<br>
            地元チームとの練習試合①</td>
        </tr>
        <tr>
          <th>Day 3</th>
          <td>UEFA公認コーチによる指導②<br>
            UEFA公認コーチによる指導③</td>
        </tr>
        <tr>
          <th>Day 4</th>
          <td>ロンドン観光<br>
            ウェンブリースタジアムツアー</td>
        </tr>
        <tr>
          <th>Day 5</th>
          <td>地元チームとの練習試合②<br>
            プロチーム試合観戦</td>
        </tr>
        <tr>
          <th>Day 6</th>
          <td>UEFA公認コーチによる指導④<br>
            地元チームとの練習試合③</td>
        </tr>
        <tr>
          <th>Day 7</th>
          <td>朝食<br>
            チェックアウト<br>
            宿舎 ⇒ 空港着<br>
            ロンドンヒースロー空港発<br>
            東京着</td>
        </tr>
      </table>
    </div>
  </div>
</div>
<div class="m-photo-box">
  <img src="<?php echo get_theme_file_uri(); ?>/assets/images/common/common_photobox_01.jpg" alt="">
  <img src="<?php echo get_theme_file_uri(); ?>/assets/images/common/common_photobox_02.jpg" alt="">
  <img src="<?php echo get_theme_file_uri(); ?>/assets/images/common/common_photobox_03.jpg" alt="">
  <img src="<?php echo get_theme_file_uri(); ?>/assets/images/common/common_photobox_04.jpg" alt="">
  <img src="<?php echo get_theme_file_uri(); ?>/assets/images/common/common_photobox_05.jpg" alt="">
  <img src="<?php echo get_theme_file_uri(); ?>/assets/images/common/common_photobox_06.jpg" alt="">
  <img src="<?php echo get_theme_file_uri(); ?>/assets/images/common/common_photobox_07.jpg" alt="">
  <img src="<?php echo get_theme_file_uri(); ?>/assets/images/common/common_photobox_08.jpg" alt="">
  <img src="<?php echo get_theme_file_uri(); ?>/assets/images/common/common_photobox_09.jpg" alt="">
  <img src="<?php echo get_theme_file_uri(); ?>/assets/images/common/common_photobox_10.jpg" alt="">
  <img src="<?php echo get_theme_file_uri(); ?>/assets/images/common/common_photobox_11.jpg" alt="">
  <img src="<?php echo get_theme_file_uri(); ?>/assets/images/common/common_photobox_12.jpg" alt="">
</div>
<?php if(have_rows('cf_page_voiceRepeat')): ?>
<div class="m-voice-box" id="voice">
  <div class="container">
    <div class="m-title04">
      <p class="en">Voice</p>
      <p class="title">参加者の声</p>
    </div>
    <?php while(have_rows('cf_page_voiceRepeat')):the_row(); ?>
    <div class="box">
      <p class="name"><?php the_sub_field('cf_page_voiceRepeat_name'); ?>さん<?php if(get_sub_field('cf_page_voiceRepeat_kana')): ?>（<?php the_sub_field('cf_page_voiceRepeat_kana'); ?>）<?php endif; ?></p>
      <p class="heading"><?php the_sub_field('cf_page_voiceRepeat_title'); ?></p>
      <p class="read"><?php the_sub_field('cf_page_voiceRepeat_read'); ?></p>
      <?php if(have_rows('cf_page_voiceRepeat_photoRepeat')): ?>
      <div class="photo">
        <?php while(have_rows('cf_page_voiceRepeat_photoRepeat')):the_row(); ?>
        <img src="<?php the_sub_field('cf_page_voiceRepeat_photoRepeat_img'); ?>" alt="">
        <?php endwhile; ?>
      </div>
      <?php endif; ?>
      <?php if(get_sub_field('cf_page_voiceRepeat_remark')): ?>
      <p class="remark"><?php the_sub_field('cf_page_voiceRepeat_remark'); ?></p>
      <?php endif; ?>
    </div>
    <?php endwhile; ?>
  </div>
</div>
<?php endif; ?>