<div class="m-nav-box">
  <div class="container">
    <ul class="flex">
      <li>
        <a href="#about">
          <span>About</span>
          <p>語学留学とは</p>
        </a>
      </li>
      <li>
        <a href="#cost">
          <span>Cost</span>
          <p>留学費用</p>
        </a>
      </li>
      <?php
      /*
      <li>
        <a href="#schedule">
          <span>Schedule</span>
          <p>スケジュール例</p>
        </a>
      </li>
      */
      ?>
      <li>
        <?php if(have_rows('cf_page_voiceRepeat')): ?>
        <a href="#voice">
          <span>Voice</span>
          <p>参加者の声</p>
        </a>
        <?php else: ?>
        <a href="#support">
          <span>Support</span>
          <p>充実したサポート</p>
        </a>
        <?php endif; ?>
      </li>
    </ul>
  </div>
</div>
<div class="m-point-box" id="about">
  <div class="container">
    <div class="box">
      <div class="img">
        <div class="border">
          <img src="<?php echo get_theme_file_uri(); ?>/assets/images/page/learn-language/img_01.jpg" alt="">
        </div>
      </div>
      <div class="text">
        <div class="heading">
          <div class="point">
            <p><span>POINT</span><strong>01</strong></p>
          </div>
          <p class="title">渡英時の英語力に自信がなくても大丈夫</p>
        </div>
        <p class="read">質の高い語学学校をご紹介します。平日は語学学校に通い語学力の向上を目指します。どの語学学校も基礎レベルからビジネスレベルまで幅広いクラスがあるのであったレベルから始めステップを踏みながら上のクラスを目指すことが可能です。</p>
      </div>
    </div>
    <div class="box">
      <div class="img">
        <div class="border">
          <img src="<?php echo get_theme_file_uri(); ?>/assets/images/page/learn-language/img_02.jpg" alt="">
        </div>
      </div>
      <div class="text">
        <div class="heading">
          <div class="point">
            <p><span>POINT</span><strong>02</strong></p>
          </div>
          <h2 class="title">語学学校外も全てが英語に囲まれた環境</h2>
        </div>
        <p class="read">留学では日本にいるのと違い語学学校の外も英語習得の場になります。その一つが居住地です。最初はホームステイが一般的ですが、慣れてくれば途中からシェアハウスに引っ越す方もいらっしゃいます。</p>
      </div>
    </div>
  </div>
</div>
<div class="m-cost-box" id="cost">
  <div class="container">
    <div class="box">
      <div class="wrap">
        <div class="m-title04">
          <p class="en">Season &amp; Cost</p>
          <p class="title">留学費用</p>
        </div>
        <p class="read">内容、時期などにより変わる為まずはお気軽にお問い合わせください。<br><span><strong>ご希望の予算からプランを組み立てる</strong></span>ことも可能です。</p>
        <div class="button">
          <a href="<?php echo home_url(); ?>/contact/" class="m-button01 big black">メールでお問い合わせはこちら</a>
        </div>
      </div>
    </div>
  </div>
</div>
<?php
/*
<div class="m-schedule-box" id="schedule">
  <div class="container">
    <div class="m-title04">
      <p class="en">Schedule</p>
      <p class="title">スケジュール例</p>
    </div>
    <div class="table">
      <table>
        <tr>
          <th></th>
          <th>Mon</th>
          <th>Tue</th>
          <th>Wed</th>
          <th>Thu</th>
          <th>Fri</th>
          <th>Sat</th>
          <th>Sun</th>
        </tr>
        <tr>
          <th>9:00～10:30</th>
          <td>語学学校授業</td>
          <td>語学学校授業</td>
          <td>語学学校授業</td>
          <td>語学学校授業</td>
          <td>語学学校授業</td>
          <td rowspan="6">休日</td>
          <td rowspan="6">休日</td>
        </tr>
        <tr>
          <th>10:30～10:45</th>
          <td>休憩</td>
          <td>休憩</td>
          <td>休憩</td>
          <td>休憩</td>
          <td>休憩</td>
        </tr>
        <tr>
          <th>10:45～12:15</th>
          <td>語学学校授業</td>
          <td>語学学校授業</td>
          <td>語学学校授業</td>
          <td>語学学校授業</td>
          <td>語学学校授業</td>
        </tr>
        <tr>
          <th>12:15～14:00</th>
          <td>ランチ・移動</td>
          <td rowspan="3">自由</td>
          <td>ランチ・移動</td>
          <td rowspan="3">自由</td>
          <td>ランチ・移動</td>
        </tr>
        <tr>
          <th>14:00～16:00</th>
          <td>現地チームにて<br>トレーニング</td>
          <td>現地チームにて<br>トレーニング</td>
          <td>現地チームにて<br>トレーニング</td>
        </tr>
        <tr>
          <th>17:00</th>
          <td>帰宅</td>
          <td>帰宅</td>
          <td>帰宅</td>
        </tr>
      </table>
    </div>
  </div>
</div>
*/
?>
<div class="m-photo-box">
  <img src="<?php echo get_theme_file_uri(); ?>/assets/images/common/common_photobox_01.jpg" alt="">
  <img src="<?php echo get_theme_file_uri(); ?>/assets/images/common/common_photobox_02.jpg" alt="">
  <img src="<?php echo get_theme_file_uri(); ?>/assets/images/common/common_photobox_03.jpg" alt="">
  <img src="<?php echo get_theme_file_uri(); ?>/assets/images/common/common_photobox_04.jpg" alt="">
  <img src="<?php echo get_theme_file_uri(); ?>/assets/images/common/common_photobox_05.jpg" alt="">
  <img src="<?php echo get_theme_file_uri(); ?>/assets/images/common/common_photobox_06.jpg" alt="">
  <img src="<?php echo get_theme_file_uri(); ?>/assets/images/common/common_photobox_07.jpg" alt="">
  <img src="<?php echo get_theme_file_uri(); ?>/assets/images/common/common_photobox_08.jpg" alt="">
  <img src="<?php echo get_theme_file_uri(); ?>/assets/images/common/common_photobox_09.jpg" alt="">
  <img src="<?php echo get_theme_file_uri(); ?>/assets/images/common/common_photobox_10.jpg" alt="">
  <img src="<?php echo get_theme_file_uri(); ?>/assets/images/common/common_photobox_11.jpg" alt="">
  <img src="<?php echo get_theme_file_uri(); ?>/assets/images/common/common_photobox_12.jpg" alt="">
</div>
<?php if(have_rows('cf_page_voiceRepeat')): ?>
<div class="m-voice-box" id="voice">
  <div class="container">
    <div class="m-title04">
      <p class="en">Voice</p>
      <p class="title">参加者の声</p>
    </div>
    <?php while(have_rows('cf_page_voiceRepeat')):the_row(); ?>
    <div class="box">
      <p class="name"><?php the_sub_field('cf_page_voiceRepeat_name'); ?>さん<?php if(get_sub_field('cf_page_voiceRepeat_kana')): ?>（<?php the_sub_field('cf_page_voiceRepeat_kana'); ?>）<?php endif; ?></p>
      <p class="heading"><?php the_sub_field('cf_page_voiceRepeat_title'); ?></p>
      <p class="read"><?php the_sub_field('cf_page_voiceRepeat_read'); ?></p>
      <?php if(have_rows('cf_page_voiceRepeat_photoRepeat')): ?>
      <div class="photo">
        <?php while(have_rows('cf_page_voiceRepeat_photoRepeat')):the_row(); ?>
        <img src="<?php the_sub_field('cf_page_voiceRepeat_photoRepeat_img'); ?>" alt="">
        <?php endwhile; ?>
      </div>
      <?php endif; ?>
      <?php if(get_sub_field('cf_page_voiceRepeat_remark')): ?>
      <p class="remark"><?php the_sub_field('cf_page_voiceRepeat_remark'); ?></p>
      <?php endif; ?>
    </div>
    <?php endwhile; ?>
  </div>
</div>
<?php endif; ?>