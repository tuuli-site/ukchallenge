<div class="p-contact-box thanks">
  <div class="m-container">
    <h2 class="m-title02 center"><span>お問い合わせいただき、<br class="nopc">ありがとうございます。</span></h2>
    <p class="tac">担当の者が確認でき次第、<br class="nopc">ご連絡させていただきます。<br>今後ともUK Challengeを、<br class="nopc">宜しくお願い申し上げます。</p>
    <div class="button">
      <a href="<?php echo home_url(); ?>" class="m-button01"><span>トップページへ戻る</span></a>
    </div>
  </div>
</div>