<div class="p-contact-box confirm">
  <div class="m-container">
    <p class="description">入力内容にお間違いなければ、<br class="nopc">送信ボタンを押してください。</p>
    <div class="form">
      <table>
        <tr>
          <th>
            <p>申込日</p>
          </th>
          <td><?php echo date('Y/m/d'); ?></td>
        </tr>
        <?php if(have_posts()):while(have_posts()):the_post(); ?>
        <?php the_content(); ?>
        <?php endwhile; endif; ?>
      </table>
    </div>
  </div>
</div>