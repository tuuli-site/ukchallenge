<div class="p-contact-box">
  <div class="m-container">
    <p class="description">留学期間や内容は予算などご希望に合わせてご提案させていただきます。<br>留学するかまだ悩んでいる、留学先はイギリスでいいのか、留学後が想像できないなど<br class="nosp">まだ留学を決心していなくてもご相談はいつでも受付けています。<br>サッカーチーム遠征や企業の研修旅行などもこちらからお問い合わせください。<br>まずはお気軽にご連絡ください。</p>
    <div class="form">
      <table>
        <tr>
          <th>
            <p>申込日</p>
          </th>
          <td><?php echo date('Y/m/d'); ?></td>
        </tr>
        <?php if(have_posts()):while(have_posts()):the_post(); ?>
        <?php the_content(); ?>
        <?php endwhile; endif; ?>
      </table>
    </div>
  </div>
</div>