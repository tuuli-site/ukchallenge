<div class="m-container">
  <dl>
    <dd>
      <a href="<?php echo home_url(); ?>" class="m-title02 imghover">TOPページ</a>
    </dd>
    <dd>
      <a href="<?php echo home_url(); ?>/laundry/" class="m-title02 imghover">ニドコインランドリー</a>
      <ul>
        <li>
          <a href="<?php echo home_url(); ?>/laundry/howto/">開業の手引き</a>
        </li>
        <li>
          <a href="<?php echo home_url(); ?>/laundry/tips/">経営のコツ</a>
        </li>
        <li>
          <a href="<?php echo home_url(); ?>/laundry/flow/">開業の手順</a>
        </li>
        <li>
          <a href="<?php echo home_url(); ?>/laundry/scale/">規模と収支予測</a>
        </li>
        <li>
          <a href="<?php echo home_url(); ?>/laundry/example/">店舗開業実例</a>
        </li>
      </ul>
    </dd>
    <dd>
      <a href="<?php echo home_url(); ?>/rental/" class="m-title02 imghover">レンタルコインランドリー</a>
      <ul>
        <li>
          <a href="<?php echo home_url(); ?>/rental/merit/">レンタルシステムのメリット</a>
        </li>
        <li>
          <a href="<?php echo home_url(); ?>/rental/flow/">導入の流れ</a>
        </li>
        <li>
          <a href="<?php echo home_url(); ?>/rental/results/">導入実績</a>
        </li>
      </ul>
    </dd>
    <dd>
      <a href="<?php echo home_url(); ?>/products/shower/" class="m-title02 imghover">コインシャワー</a>
      <ul>
        <li>
          <a href="<?php echo home_url(); ?>/shower/">コインシャワーについて</a>
        </li>
      </ul>
    </dd>
    <dd>
      <a href="<?php echo home_url(); ?>/pet/" class="m-title02 imghover">ペットシャワー</a>
    </dd>
    <dd>
      <span class="m-title02">その他</span>
      <ul>
        <li>
          <a href="<?php echo home_url(); ?>/company/">会社概要</a>
        </li>
        <li>
          <a href="<?php echo home_url(); ?>/message/">社長挨拶</a>
        </li>
        <li>
          <a href="<?php echo home_url(); ?>/contact/">お問い合わせ</a>
        </li>
        <li>
          <a href="<?php echo home_url(); ?>/privacy/">個人情報保護方針</a>
        </li>
      </ul>
    </dd>
  </dl>
</div>