<div class="m-nav-box">
  <div class="container">
    <ul class="flex">
      <li>
        <a href="#about">
          <span>About</span>
          <p>アート・音楽留学とは</p>
        </a>
      </li>
      <li>
        <a href="#cost">
          <span>Cost</span>
          <p>留学費用</p>
        </a>
      </li>
      <li>
        <a href="#voice">
          <span>Voice</span>
          <p>参加者の声</p>
        </a>
      </li>
      <li>
        <a href="#support">
          <span>Support</span>
          <p>充実したサポート</p>
        </a>
      </li>
    </ul>
  </div>
</div>
<div class="m-point-box" id="about">
  <div class="container">
    <div class="box">
      <div class="img">
        <div class="border">
          <img src="<?php echo get_theme_file_uri(); ?>/assets/images/page/art-music/img_01.jpg" alt="">
        </div>
      </div>
      <div class="text">
        <div class="heading">
          <div class="point">
            <p><span>POINT</span><strong>01</strong></p>
          </div>
          <p class="title">一人一人に合わせたアート留学</p>
        </div>
        <p class="read">デッサン、絵画、彫刻、写真、グラフィック、油絵、デジタルなど様々なジャンルを学ぶことが可能です。<br>午前中に語学学校に通い、午後にアートスクールでそれぞれのレベルに合わせた専門技術を学びます。</p>
      </div>
    </div>
    <div class="box">
      <div class="img">
        <div class="border">
          <img src="<?php echo get_theme_file_uri(); ?>/assets/images/page/art-music/img_02.jpg" alt="">
        </div>
      </div>
      <div class="text">
        <div class="heading">
          <div class="point">
            <p><span>POINT</span><strong>02</strong></p>
          </div>
          <h2 class="title">ロンドン芸大を目指すなど<br>本格的な留学も可能</h2>
        </div>
        <p class="read">高校生の年代であればアートのボーディングスクールに入学することも可能です。紹介先の中には世界中から学生が集まるロンドンの芸術大学を目指す学校もあります。また、日本の高校卒業資格が取れる学校など一人一人にあった留学のご紹介が可能です。</p>
      </div>
    </div>
    <div class="box">
      <div class="img">
        <div class="border">
          <img src="<?php echo get_theme_file_uri(); ?>/assets/images/page/art-music/img_03.jpg" alt="">
        </div>
      </div>
      <div class="text">
        <div class="heading">
          <div class="point">
            <p><span>POINT</span><strong>03</strong></p>
          </div>
          <p class="title">最適な音楽留学プランを作成</p>
        </div>
        <p class="read">ボーカル、ギター、ドラム、ベース、バイオリン、サックスフォンなど様々なジャンルを学ぶことが可能です。レッスン形式は好みに合わせて選択することができます。午前中に語学学校に通い、午後にミュージックスクールで専門技術を学びます。</p>
      </div>
    </div>
    <div class="box">
      <div class="img">
        <div class="border">
          <img src="<?php echo get_theme_file_uri(); ?>/assets/images/page/art-music/img_04.jpg" alt="">
        </div>
      </div>
      <div class="text">
        <div class="heading">
          <div class="point">
            <p><span>POINT</span><strong>04</strong></p>
          </div>
          <h2 class="title">イギリスで世界水準の音楽を学ぶ</h2>
        </div>
        <p class="read">初心者から上級者まで一人一人に合わせたプランの作成が可能です。世界的なミュージックスターを輩出し続けるイギリスで様々な表現方法に触れながら技術を向上させることができます。</p>
      </div>
    </div>
  </div>
</div>
<div class="m-cost-box" id="cost">
  <div class="container">
    <div class="box">
      <div class="wrap">
        <div class="m-title04">
          <p class="en">Season &amp; Cost</p>
          <p class="title">留学費用</p>
        </div>
        <p class="read">内容、時期などにより変わる為まずはお気軽にお問い合わせください。<br><span><strong>ご希望の予算からプランを組み立てる</strong></span>ことも可能です。</p>
        <div class="button">
          <a href="<?php echo home_url(); ?>/contact/" class="m-button01 big black">メールでお問い合わせはこちら</a>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="m-photo-box">
  <img src="<?php echo get_theme_file_uri(); ?>/assets/images/common/common_photobox_01.jpg" alt="">
  <img src="<?php echo get_theme_file_uri(); ?>/assets/images/common/common_photobox_02.jpg" alt="">
  <img src="<?php echo get_theme_file_uri(); ?>/assets/images/common/common_photobox_03.jpg" alt="">
  <img src="<?php echo get_theme_file_uri(); ?>/assets/images/common/common_photobox_04.jpg" alt="">
  <img src="<?php echo get_theme_file_uri(); ?>/assets/images/common/common_photobox_05.jpg" alt="">
  <img src="<?php echo get_theme_file_uri(); ?>/assets/images/common/common_photobox_06.jpg" alt="">
  <img src="<?php echo get_theme_file_uri(); ?>/assets/images/common/common_photobox_07.jpg" alt="">
  <img src="<?php echo get_theme_file_uri(); ?>/assets/images/common/common_photobox_08.jpg" alt="">
  <img src="<?php echo get_theme_file_uri(); ?>/assets/images/common/common_photobox_09.jpg" alt="">
  <img src="<?php echo get_theme_file_uri(); ?>/assets/images/common/common_photobox_10.jpg" alt="">
  <img src="<?php echo get_theme_file_uri(); ?>/assets/images/common/common_photobox_11.jpg" alt="">
  <img src="<?php echo get_theme_file_uri(); ?>/assets/images/common/common_photobox_12.jpg" alt="">
</div>
<?php if(have_rows('cf_page_voiceRepeat')): ?>
<div class="m-voice-box" id="voice">
  <div class="container">
    <div class="m-title04">
      <p class="en">Voice</p>
      <p class="title">参加者の声</p>
    </div>
    <?php while(have_rows('cf_page_voiceRepeat')):the_row(); ?>
    <div class="box">
      <p class="name"><?php the_sub_field('cf_page_voiceRepeat_name'); ?>さん<?php if(get_sub_field('cf_page_voiceRepeat_kana')): ?>（<?php the_sub_field('cf_page_voiceRepeat_kana'); ?>）<?php endif; ?></p>
      <p class="heading"><?php the_sub_field('cf_page_voiceRepeat_title'); ?></p>
      <p class="read"><?php the_sub_field('cf_page_voiceRepeat_read'); ?></p>
      <?php if(have_rows('cf_page_voiceRepeat_photoRepeat')): ?>
      <div class="photo">
        <?php while(have_rows('cf_page_voiceRepeat_photoRepeat')):the_row(); ?>
        <img src="<?php the_sub_field('cf_page_voiceRepeat_photoRepeat_img'); ?>" alt="">
        <?php endwhile; ?>
      </div>
      <?php endif; ?>
      <?php if(get_sub_field('cf_page_voiceRepeat_remark')): ?>
      <p class="remark"><?php the_sub_field('cf_page_voiceRepeat_remark'); ?></p>
      <?php endif; ?>
    </div>
    <?php endwhile; ?>
  </div>
</div>
<?php endif; ?>