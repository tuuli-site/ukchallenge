<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie6" lang="ja"> <![endif]-->
<!--[if IE 7]> <html class="ie7" lang="ja"> <![endif]-->
<!--[if IE 8]> <html class="ie8" lang="ja"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="ja"> <!--<![endif]-->
  <head>
    <meta charset="utf-8">
    <!--[if lt IE 9]><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><![endif]-->
    <meta id="viewport" name='viewport'>
    <script type="text/javascript">
      (function(doc) {
        var viewport = document.getElementById('viewport');
        if ( navigator.userAgent.match(/iPad/i) ) {
          viewport.setAttribute("content", "initial-scale=0.6");
        } else {
          viewport.setAttribute("content", "width=device-width, initial-scale=1.0");
        }
      }(document));
    </script>
    <meta name="format-detection" content="telephone=no">

    <link rel="shortcut icon" href="<?php echo get_theme_file_uri(); ?>/assets/images/common/favicon.ico">
    <link rel="icon" type="image/vnd.microsoft.icon" href="<?php echo get_theme_file_uri(); ?>/assets/images/common/favicon.ico">
    
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">

    <?php
    if(is_tax()){
      $tax_slug = get_query_var('taxonomy');                  // 現在のページのタクソノミースラッグ
      $term_slug = get_query_var('term');                     // 現在のページのタームスラッグ
      $term_meta = get_term_by("slug",$term_slug,$tax_slug);  // 現在のページのターム情報を取得
      $term_id = $term_meta->term_id;                         // 現在のページのタームID
      $term_name = $term_meta->name;                          // 現在のページのターム名
    }
    if(is_post_type_archive()){
      // 一覧ページ用の記述
      $post_type = get_query_var('post_type');
      $post_type_meta = get_post_type_object($post_type);
      $post_type_slug = $post_type_meta->name;
      $post_type_name = $post_type_meta->label;
    }elseif(is_tax()){
      // カテゴリー一覧ページ用の記述
      $post_tax = get_query_var('taxonomy');
      $post_type_slug = get_taxonomy($post_tax)->object_type[0];
    }elseif(is_single()){
      // 詳細ページ用の記述
      $post_type_slug = get_post_type();
    }
    ?>

    <!-- title -->
    <?php if(is_404()): ?><title>お探しのページは見つかりません。｜<?php bloginfo('name'); ?></title>
    <?php elseif(is_home() || is_front_page()): ?><title><?php bloginfo('name'); ?></title>
    <?php elseif(is_post_type_archive()): ?><title><?php echo post_type_archive_title('', false); ?>｜<?php bloginfo('name'); ?></title>
    <?php elseif(is_tax()): ?><title><?php echo $term_name; ?>｜<?php echo post_type_archive_title('', false); ?>｜<?php bloginfo('name'); ?></title>
    <?php elseif(is_single()): ?><title><?php the_title(); ?>｜<?php echo get_post_type_object(get_post_type())->label; ?>｜<?php bloginfo('name'); ?></title>
    <?php else: ?><title><?php the_title(); ?>｜<?php bloginfo('name'); ?></title>
    <?php endif; ?>

    <!-- description -->
    <?php if(is_404()): ?><meta name="description" content="404エラーページです。">
    <?php elseif(is_post_type_archive()): ?><meta name="description" content="<?php echo post_type_archive_title('', false); ?>ページです。">
    <?php elseif(is_tax()): ?><meta name="description" content="<?php echo $term_name; ?>カテゴリー一覧ページです。">
    <?php else: ?><meta name="description" content="<?php bloginfo('description'); ?>">
    <?php endif; ?>

    <!-- keywords -->
    <?php if(is_404()): ?><meta name="keywords" content="404エラー">
    <?php else: ?><meta name="keywords" content="">
    <?php endif; ?>

    <!-- canonical -->
    <?php
    // 【変数】現在ページのURL取得用
    $http = is_ssl() ? 'https://' : 'http://';
    $url = $http . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
    ?>
    <?php
    // 【分岐】ページ送り対策
    if(strstr($url,'/page/') == true): ?>
    <link rel="canonical" href="<?php echo htmlspecialchars(strstr($url,'/page/',true)) . '/'; ?>">
    <?php else: ?>
    <link rel="canonical" href="<?php echo htmlspecialchars($url); ?>">
    <?php endif; ?>

    <link rel="stylesheet" type="text/css" href="<?php echo get_theme_file_uri(); ?>/assets/css/common/reset.css?v=1" media="all">
    <link rel="stylesheet" type="text/css" href="<?php echo get_theme_file_uri(); ?>/assets/css/common/base.css?v=1" media="all">
    <link rel="stylesheet" type="text/css" href="<?php echo get_theme_file_uri(); ?>/assets/css/common/common.css?v=1" media="all">
    <link rel="stylesheet" type="text/css" href="<?php echo get_theme_file_uri(); ?>/assets/css/common/module.css?v=1" media="all">
    <link rel="stylesheet" type="text/css" href="<?php echo get_theme_file_uri(); ?>/assets/css/common/print.css" media="print">
    <link rel="stylesheet" type="text/css" href="<?php echo get_theme_file_uri(); ?>/assets/css/common/animate.css?v=1" media="all">

    <script type="text/javascript" src="<?php echo get_theme_file_uri(); ?>/assets/js/common/jquery-1.8.3.js?v=1"></script>
    <script type="text/javascript" src="<?php echo get_theme_file_uri(); ?>/assets/js/common/jquery.easing.1.3.js?v=1"></script>
    <script type="text/javascript" src="<?php echo get_theme_file_uri(); ?>/assets/js/common/css_browser_selector.js?v=1"></script>
    <script type="text/javascript" src="<?php echo get_theme_file_uri(); ?>/assets/js/common/jquery.matchHeight.min.js?v=1"></script>
    <script type="text/javascript" src="<?php echo get_theme_file_uri(); ?>/assets/js/common/imgLiquid-min.js?v=1"></script>
    <script type="text/javascript" src="<?php echo get_theme_file_uri(); ?>/assets/js/common/wow.js?v=1"></script>
    <script type="text/javascript">
      new WOW({
        mobile: false
      }).init();
    </script>
    <script type="text/javascript" src="<?php echo get_theme_file_uri(); ?>/assets/js/common/common.js?v=1"></script>
    <!--[if lt IE 9]>
<script type="text/javascript" src="<?php echo get_theme_file_uri(); ?>/assets/js/common/html5shiv.js?v=1"></script>
<![endif]-->

    <?php
    // 【変数】ページ固有のCSS,JSファイル取得用
    $page = get_post( get_the_ID() ); // 現在のページの情報を投稿IDから取得
    $slug = $page->post_name; // 現在のページのスラッグを取得
    $ancestors = get_post_ancestors( $post->ID ); // すべての先祖ページの投稿IDを配列で取得
    $ancestors = array_reverse ( $ancestors ); // 取得した先祖ページの投稿IDを逆順にする
    $parent_slug = '';
    foreach ( $ancestors as $ancestor ) {
      $parent_slug .= '/'.get_post($ancestor)->post_name; // 先祖ページのスラッグを取得
    }
    $link_slug = $parent_slug.'/'.$slug.'/'.$slug;

    // 【変数】現在ページの投稿タイプスラッグ取得用
    if(is_post_type_archive()){
      // 一覧ページ用の記述
      $post_type = get_query_var('post_type');
      $post_type_meta = get_post_type_object($post_type);
      $post_type_slug = $post_type_meta->name;
      $post_type_name = $post_type_meta->label;
    }elseif(is_tax()){
      // カテゴリー一覧ページ用の記述
      $post_tax = get_query_var('taxonomy');
      $post_type_slug = get_taxonomy($post_tax)->object_type[0];
    }elseif(is_single()){
      // 詳細ページ用の記述
      $post_type_slug = get_post_type();
      $post_type_name = get_post_type()->label;
    }
    ?>

    <?php if(is_404()): ?>
    <link rel="stylesheet" type="text/css" href="<?php echo get_theme_file_uri(); ?>/assets/css/page/404/404.css" media="all">
    <?php
    // 【分岐】トップページの場合
    elseif(is_home() || is_front_page()): ?>
    <link rel="stylesheet" type="text/css" href="<?php echo get_theme_file_uri().'/assets/css/top/top.css?v=1'; ?>" media="all">
    <?php
    // 【分岐】投稿一覧・詳細ページの場合
    elseif(is_post_type_archive($post_type_slug) || get_post_type() == $post_type_slug): ?>
    <link rel="stylesheet" type="text/css" href="<?php echo get_theme_file_uri().'/assets/css/post/'.$post_type_slug.'/'.$post_type_slug.'.css?v=1'; ?>" media="all">
    <?php
    /// 【分岐】お問い合わせページの場合
    elseif(strstr($url,'/contact/') == true): ?>
    <link rel="stylesheet" type="text/css" href="<?php echo get_theme_file_uri().'/assets/css/page/contact/contact.css?v=1'; ?>" media="all">
    <?php
    // 【分岐】固定ページの場合
    elseif(is_page()): ?>
    <?php if(file_exists(get_template_directory().'/assets/css/page'.$link_slug.'.css')): ?>
    <link rel="stylesheet" type="text/css" href="<?php echo get_theme_file_uri().'/assets/css/page'.$link_slug.'.css?v=1'; ?>" media="all">
    <?php endif; ?>
    <?php endif; ?>

    <?php
    // 【分岐】404ページの場合
    if(is_404()): ?><script type="text/javascript" src="<?php echo get_theme_file_uri().'/assets/js/page/404/404.js?v=1'; ?>"></script>
    <?php
    // 【分岐】トップページの場合
    elseif(is_home() || is_front_page()): ?>
    <script type="text/javascript" src="<?php echo get_theme_file_uri().'/assets/js/top/top.js?v=1'; ?>"></script>
    <?php
    // 【分岐】投稿一覧・詳細ページの場合
    elseif(is_post_type_archive($post_type_slug) || get_post_type() == $post_type_slug): ?>
    <script type="text/javascript" src="<?php echo get_theme_file_uri().'/assets/js/post/'.$post_type_slug.'/'.$post_type_slug.'.js?v=1'; ?>"></script>
    <?php
    // 【分岐】固定ページの場合
    elseif(is_page()): ?>
    <?php if(file_exists(get_template_directory().'/assets/js/page'.$link_slug.'.js')): ?>
    <script type="text/javascript" src="<?php echo get_theme_file_uri().'/assets/js/page'.$link_slug.'.js?v=1'; ?>"></script>
    <?php endif; ?>
    <?php endif; ?>
    
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-173568187-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-173568187-1');
    </script>

    <?php wp_head(); ?>
  </head>

  <body id="pagetop">

    <?php
    if(is_home() || is_front_page()){
      $add_home_class = ' home';
    }
    else{
      $add_home_class = '';
    }
    ?>

    <header class="l-header<?php echo $add_home_class; ?>">
      <div class="flex">
        <div class="logo">
          <a href="<?php echo home_url(); ?>">
            <img src="<?php echo get_theme_file_uri(); ?>/assets/images/common/header_img_logo.svg" alt="<?php bloginfo('home'); ?>">
          </a>
        </div>
      </div>
    </header>
    
    <div class="l-toggle">
      <span></span>
      <span></span>
      <span></span>
    </div>

    <nav class="l-nav<?php echo $add_home_class; ?>">
      <div class="flex">
        <div class="links">
          <?php
          if(is_home() || is_front_page()){
            $add_nav_link = '';
          }
          else{
            $add_nav_link = home_url('/');
          }
          ?>
          <ul class="flex">
            <li>
              <a href="<?php echo home_url(); ?>">
                <span>HOME</span>
              </a>
            </li>
            <li>
              <a href="<?php echo $add_nav_link; ?>#about">
                <span>UK Challengeとは</span>
              </a>
            </li>
            <li>
              <a href="<?php echo $add_nav_link; ?>#genre">
                <span>留学ジャンル</span>
              </a>
            </li>
            <li>
              <a href="<?php echo $add_nav_link; ?>blog/">
                <span>ブログ</span>
              </a>
            </li>
            <li>
              <a href="<?php echo $add_nav_link; ?>news/">
                <span>お知らせ</span>
              </a>
            </li>
            <li>
              <a href="<?php echo $add_nav_link; ?>company/">
                <span>事業概要</span>
              </a>
            </li>
            <li>
              <a href="<?php echo $add_nav_link; ?>contact/">
                <span>お問い合わせ</span>
              </a>
            </li>
          </ul>
        </div>
        <div class="sns">
          <ul class="flex">
            <li>
              <a href="https://twitter.com/ukchallenge2020" target="_blank"></a>
            </li>
            <li>
              <a href="https://www.instagram.com/uk_challenge/?hl=ja" target="_blank"></a>
            </li>
            <li>
              <a href="https://www.facebook.com/UK-CHALLENGE-102313288209608/" target="_blank"></a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <div class="l-pageBody<?php echo $add_home_class; ?>">
      <?php if(is_home() || is_front_page()): ?>
      <?php elseif(is_404() || is_post_type_archive() || is_tax() || is_single() || is_page(array('football','company','contact','confirm','thanks'))): ?>
      <div class="l-mv p-mv">
      <div class="m-container">
        <div>
          <?php if(is_404()): ?>
          <p class="en">404</p>
          <h1 class="title">404 Not Found.</h1>
          <?php elseif(is_post_type_archive('blog') || is_tax('blog_cat')): ?>
          <p class="en">Blog</p>
          <h1 class="title">ブログ</h1>
          <?php elseif(is_singular('blog')): ?>
          <p class="en">Blog</p>
          <p class="title">ブログ</p>
          <?php elseif(is_post_type_archive('news')): ?>
          <p class="en">News</p>
          <h1 class="title">お知らせ</h1>
          <?php elseif(is_singular('news')): ?>
          <p class="en">News</p>
          <p class="title">お知らせ</p>
          <?php elseif(is_page(array('contact','confirm','thanks'))): ?>
          <p class="en"><?php the_field('cf_page_en'); ?></p>
          <p class="title">お問い合わせ</p>
          <?php else: ?>
          <p class="en"><?php the_field('cf_page_en'); ?></p>
          <h1 class="title"><?php the_title(); ?></h1>
          <?php endif; ?>
        </div>
      </div>
      </div>
      <?php else: ?>
      <div class="m-mv p-mv">
        <div class="container">
          <div class="box">
            <p class="en"><?php the_field('cf_page_en'); ?></p>
            <p class="title"><?php the_title(); ?></p>
            <p class="read"><?php the_field('cf_page_read'); ?></p>
          </div>
        </div>
      </div>
      <?php endif; ?>
      <?php if(is_home() || is_front_page()): ?>
      <?php else: ?>
      <div class="l-box p-box">
        <?php endif; ?>