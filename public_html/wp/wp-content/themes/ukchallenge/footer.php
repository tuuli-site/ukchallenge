<?php if(!is_home() && is_front_page()): ?>
</div><!-- /.p-box -->
<?php endif; ?>

<?php if(is_home() || is_front_page() || is_page()): ?>
<?php if(!is_page(array('football','company','contact','confirm','thanks'))): ?>
<div class="m-support-box" id="support">
  <div class="m-container">
    <div class="m-title01">
      <div class="icon">
        <img src="<?php echo get_theme_file_uri(); ?>/assets/images/common/common_support_icn.svg" alt="充実のサポート アイコン">
      </div>
      <h2 class="title">充実のサポート</h2>
    </div>
    <div class="flex">
      <div class="img">
        <div class="border">
          <img src="<?php echo get_theme_file_uri(); ?>/assets/images/common/common_support_img_01.jpg" alt="安心の現地サポート イメージ">
        </div>
      </div>
      <div class="text">
        <h3 class="heading">安心の<span><strong>現地サポート</strong></span></h3>
        <p class="description">UK CHALLENGEで手配する留学は語学、スポーツ、生活面などあらゆる面で現地でのサポートを行っています。不安なことやトラブルがある際はいつでも現地担当者にご相談いただけます。UK CHALLENGEの提携先には英語はもちろん、サッカーやイギリス事情に精通したスタッフが揃っているので安心して留学ライフを楽しんでいただけます。</p>
      </div>
    </div>
    <div class="flex">
      <div class="img">
        <div class="border">
          <img src="<?php echo get_theme_file_uri(); ?>/assets/images/common/common_support_img_02.jpg" alt="ビザや保険の準備を無料でサポート イメージ">
        </div>
      </div>
      <div class="text">
        <h3 class="heading">ビザや保険の準備を<br><span><strong>徹底サポート</strong></span></h3>
        <p class="description">ビザ取得や海外保険など留学前はわからないことがたくさんあり、初めての留学前はみんな不安になります。UK CHALLENGEではビザ取得など渡英前の準備を最初から最後まで丁寧にサポートさせていただきます。部分的なサポートなどニーズに合わせた選択肢をご用意していますのでご相談ください。不安がない状態で渡英できるように全力でサポートいたします。</p>
      </div>
    </div>
    <div class="flex">
      <div class="img">
        <div class="border">
          <img src="<?php echo get_theme_file_uri(); ?>/assets/images/common/common_support_img_03.jpg" alt="留学後の進路や就職の相談 イメージ">
        </div>
      </div>
      <div class="text">
        <h3 class="heading"><span><strong>留学後の進路や就職</strong></span>の相談</h3>
        <p class="description">留学後の進路や就職については実際にイギリス留学経験があるスタッフにご相談いただけます。留学中に帰国後の進路を決めたい方もいれば、帰国後にゆっくり考える方もいます。今後は語学留学、サッカー選手留学、指導者留学など各イギリス留学経験者が集まる座談会の開催も企画していくのでその中でご相談いただけるようにしていきます。</p>
      </div>
    </div>
  </div>
</div>

<div class="m-inquiry-box" id="inquiry">
  <div class="m-container">
    <div class="box wow fadeInUp">
      <div class="container">
        <div class="m-title01">
          <div class="icon">
            <img src="<?php echo get_theme_file_uri(); ?>/assets/images/common/common_inquiry_icn.svg" alt="無料相談、随時受付中 アイコン">
          </div>
          <h2 class="title">
            <img src="<?php echo get_theme_file_uri(); ?>/assets/images/common/common_inquiry_ttl.png" alt="無料相談、随時受付中">
          </h2>
        </div>
        <p class="description">留学期間や内容は予算などご希望に合わせてご提案させていただきます。<br>留学するかまだ悩んでいる、留学先はイギリスでいいのか、留学後が想像できないなどまだ留学を決心していなくてもご相談はいつでも受付けています。<br>サッカーチーム遠征や企業の研修旅行などもこちらからお問い合わせください。<br>まずはお気軽にご連絡ください。</p>
        <div class="button">
          <div class="flex">
            <a href="https://line.me/R/ti/p/%40169jdhtt" target="_blank" class="m-button01 black big">
              <span>LINE@</span>
            </a>
            <a href="<?php echo home_url(); ?>/contact/" class="m-button01 black big">
              <span>メールでお問い合わせ</span>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php endif; ?>

<?php
if(is_home() || is_front_page() || is_page(array('player','leader','trainer','business','football-trip','art-music','learn-language','intern-travel','other'))){
  $add_fixed_link = '#inquiry';
}
else{
  $add_fixed_link = home_url().'/contact/';
}
?>
<?php if(!is_page(array('contact','confirm','thanks'))): ?>
<div class="m-sideFixed-box">
  <a href="<?php echo $add_fixed_link; ?>">
    <img src="<?php echo get_theme_file_uri(); ?>/assets/images/common/common_sidefixed_img.png" alt="無料相談実施中">
  </a>
</div>
<?php endif; ?>
<?php endif; ?>

</div><!-- /.l-pageBody -->

<?php
/*
<div class="l-pageTop">
  <a href="#pagetop" class="imghover"></a>
</div>
*/
?>

<?php
if(is_home() || is_front_page()){
  $add_page_class = ' home';
}
else{
  $add_page_class = ' page';
}
?>
<footer class="l-footer<?php echo $add_page_class; ?>">
  <div class="m-container">
    <div class="flex">
      <div class="logo">
        <a href="<?php echo home_url(); ?>">
          <img src="<?php echo get_theme_file_uri(); ?>/assets/images/common/footer_img_logo.svg" alt="<?php bloginfo('home'); ?>">
        </a>
      </div>
      <div class="links">
        <ul>
          <li>
            <a href="<?php echo home_url(); ?>">
              <span>HOME</span>
            </a>
          </li>
          <li>
            <a href="<?php echo home_url(); ?>/#about">
              <span>UK Challengeとは</span>
            </a>
          </li>
          <li>
            <a href="<?php echo home_url(); ?>/#genre">
              <span>留学ジャンル</span>
            </a>
          </li>
          <li>
            <a href="<?php echo home_url(); ?>/blog/">
              <span>ブログ</span>
            </a>
          </li>
          <li>
            <a href="<?php echo home_url(); ?>/news/">
              <span>お知らせ</span>
            </a>
          </li>
          <li>
            <a href="<?php echo home_url(); ?>/company/">
              <span>事業概要</span>
            </a>
          </li>
          <li>
            <a href="<?php echo home_url(); ?>/contact/">
              <span>お問い合わせ</span>
            </a>
          </li>
        </ul>
      </div>
    </div>
    <div class="copyright">
      <p>&copy; UK challenge</p>
    </div>
  </div>
</footer>

<?php wp_footer(); ?>
</body>
</html>