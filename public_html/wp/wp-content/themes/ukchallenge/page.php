<?php get_header(); ?>
<?php
$page = get_post(get_the_ID());
$slug = $page->post_name;
$ancestors = get_post_ancestors($post->ID);
$ancestors = array_reverse($ancestors);
$parent_slug = '';
foreach($ancestors as $ancestor){
	$parent_slug .= '/'.get_post($ancestor)->post_name;
}
$link_slug = 'page'.$parent_slug.'/'.$slug.'/index';
?>

<?php if(is_home() || is_front_page()): ?>
<?php get_template_part('index'); ?>
<?php elseif(is_page()): ?>
<?php get_template_part($link_slug); ?>
<?php endif; ?>
<?php get_footer(); ?>