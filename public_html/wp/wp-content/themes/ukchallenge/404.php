<?php get_header(); ?>
<div class="m-container">
  <h2 class="m-title02 center"><span>お探しのページは<br class="nopc">見つかりませんでした。</span></h2>
  <p class="tac">あなたがアクセスしようとしたページは<br>削除されたかURLが変更されているため、<br>見つけることができません。</p>
  <div class="button">
    <a href="<?php echo home_url(); ?>" class="m-button01"><span>トップページへ戻る</span></a>
  </div>
</div>
<?php get_footer(); ?>