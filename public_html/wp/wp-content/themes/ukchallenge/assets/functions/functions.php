<?php

/*	自動バックグラウンド更新を無効化する
/*---------------------------------------------------------*/
add_filter( 'automatic_updater_disabled', '__return_true' );


/*	アップデート通知を非表示にする
/*---------------------------------------------------------*/
function remove_update(){
  remove_action( 'admin_notices', 'update_nag', 3 );
  remove_action( 'admin_notices', 'maintenance_nag', 10 );
  remove_action( 'load-update-core.php', 'wp_update_plugins' );
  remove_action( 'load-update-core.php', 'wp_update_themes' );
}
add_action( 'admin_init', 'remove_update' );


/* 【外観】→【テーマの編集】を非表示にする
-------------------------------------------------------*/
function remove_submenus(){
  remove_submenu_page('themes.php','theme-editor.php');
}
add_filter('custom_menu_order', 'remove_submenus'); // Activate remove_submenus
add_filter('menu_order', 'remove_submenus');


/* 管理バーの項目を非表示にする
-------------------------------------------------------*/
function hide_admin_logo(){
  global $wp_admin_bar;
  $wp_admin_bar->remove_menu( 'wp-logo' );            // ロゴ
  // $wp_admin_bar->remove_menu( 'site-name' );          // サイト名
  // $wp_admin_bar->remove_menu( 'view-site' );          // サイト名 -> サイトを表示
  // $wp_admin_bar->remove_menu( 'dashboard' );          // サイト名 -> ダッシュボード (公開側)
  $wp_admin_bar->remove_menu( 'themes' );             // サイト名 -> テーマ (公開側)
  $wp_admin_bar->remove_menu( 'widgets' );            // サイト名 -> ウィジェット (公開側)
  $wp_admin_bar->remove_menu( 'menus' );              // サイト名 -> メニュー (公開側)
  $wp_admin_bar->remove_menu( 'customize' );          // サイト名 -> カスタマイズ (公開側)
  $wp_admin_bar->remove_menu( 'comments' );           // コメント
  $wp_admin_bar->remove_menu( 'updates' );            // 更新
  // $wp_admin_bar->remove_menu( 'view' );               // 投稿を表示
  $wp_admin_bar->remove_menu( 'new-content' );        // 新規
  $wp_admin_bar->remove_menu( 'new-post' );           // 新規 -> 投稿
  $wp_admin_bar->remove_menu( 'new-media' );          // 新規 -> メディア
  $wp_admin_bar->remove_menu( 'new-link' );           // 新規 -> リンク
  $wp_admin_bar->remove_menu( 'new-page' );           // 新規 -> 固定ページ
  $wp_admin_bar->remove_menu( 'new-user' );           // 新規 -> ユーザー
  // $wp_admin_bar->remove_menu( 'edit' );               // 編集
  // $wp_admin_bar->remove_menu( 'my-account' );         // マイアカウント
  // $wp_admin_bar->remove_menu( 'user-info' );          // マイアカウント -> プロフィール
  $wp_admin_bar->remove_menu( 'edit-profile' );       // マイアカウント -> プロフィール編集
  // $wp_admin_bar->remove_menu( 'logout' );             // マイアカウント -> ログアウト
  $wp_admin_bar->remove_menu( 'search' );             // 検索 (公開側)
}
add_action( 'wp_before_admin_bar_render', 'hide_admin_logo' );


/* 管理画面サイドメニューの項目を非表示にする
-------------------------------------------------------*/
function remove_menus(){
  // remove_menu_page( 'index.php' );                  // ダッシュボード
  // remove_menu_page( 'edit.php' );                   // 投稿
  // remove_menu_page( 'upload.php' );                 // メディア
  // remove_menu_page( 'edit.php?post_type=page' );    // 固定ページ
  remove_menu_page( 'edit-comments.php' );             // コメント
  // remove_menu_page( 'themes.php' );                 // 外観
  // remove_menu_page( 'plugins.php' );                // プラグイン
  // remove_menu_page( 'users.php' );                  // ユーザー
  // remove_menu_page( 'tools.php' );                  // ツール
  // remove_menu_page( 'options-general.php' );        // 設定
}
add_action('admin_menu', 'remove_menus');


/*	不要なウィジェット項目を非表示にする
/*---------------------------------------------------------*/
function unregister_default_widget() {
  unregister_widget('WP_Widget_Pages');            // 固定ページ
  //    unregister_widget('WP_Widget_Calendar');         // カレンダー
  unregister_widget('WP_Widget_Archives');         // アーカイブ
  unregister_widget('WP_Widget_Meta');             // メタ情報
  //    unregister_widget('WP_Widget_Search');           // 検索
  //    unregister_widget('WP_Widget_Text');             // テキスト
  //    unregister_widget('WP_Widget_Categories');       // カテゴリー
  //    unregister_widget('WP_Widget_Recent_Posts');     // 最近の投稿
  unregister_widget('WP_Widget_Recent_Comments');  // 最近のコメント
  //    unregister_widget('WP_Widget_RSS');              // RSS
  //    unregister_widget('WP_Widget_Tag_Cloud');        // タグクラウド
  //    unregister_widget('WP_Nav_Menu_Widget');         // カスタムメニュー
  unregister_widget( 'Akismet_Widget' );           // Akismetウィジェット
}
add_action( 'widgets_init', 'unregister_default_widget' );


/*	wp_head(); で自動的に出力される余分なタグを削除する
/*---------------------------------------------------------*/
//ブラウザが先読みするために使うタグ
// remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 ); // 親投稿 link rel="up" [depricated]
// remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );  // ルートの親投稿 link rel="start" [depricated]
// remove_action( 'wp_head', 'adjacent_posts_rel_link', 10, 0 ); // link rel="next" & link rel="prev"
// remove_action('wp_head', 'adjacent_posts_rel_link_wp_head'); // link rel=next,prev
//rel="canonical"の表示
// remove_action('wp_head', 'rel_canonical');
//コメントのフィード
remove_action('wp_head', 'feed_links_extra', 3);
//現在の文書に対する索引（インデックス）を示すリンクタグ
// remove_action('wp_head', 'index_rel_link');
//デフォルト形式のURL(?p=投稿ID)を明示するタグ
// remove_action('wp_head', 'wp_shortlink_wp_head');
//EditURI
remove_action( 'wp_head', 'rsd_link' );
//wlwmanifest
remove_action( 'wp_head', 'wlwmanifest_link' );
//generator
remove_action( 'wp_head', 'wp_generator' );
//shortlink
// remove_action( 'wp_head', 'wp_shortlink_wp_head' );
//oEmbed関連
// remove_action( 'wp_head', 'rest_output_link_wp_head' );
// remove_action( 'wp_head', 'wp_oembed_add_discovery_links' );
// remove_action( 'wp_head', 'wp_oembed_add_host_js' );
//Emoji関連
function disable_emojis() {
  remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
  remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
  remove_action( 'wp_print_styles', 'print_emoji_styles' );
  remove_action( 'admin_print_styles', 'print_emoji_styles' );     
  remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
  remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );  
  remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
  // add_filter( 'tiny_mce_plugins', 'disable_emojis_tinymce' );
}
add_action( 'init', 'disable_emojis' );


/*	レンダリングブロックしているJavascriptの読み込みを遅らせる
/*---------------------------------------------------------*/
function move_scripts_head_to_footer_ex(){
  //ヘッダーのスクリプトを取り除く
  remove_action('wp_head', 'wp_print_scripts');
  remove_action('wp_head', 'wp_print_head_scripts', 9);
  remove_action('wp_head', 'wp_enqueue_scripts', 1);

  //フッターにスクリプトを移動する
  add_action('wp_footer', 'wp_print_scripts', 5);
  add_action('wp_footer', 'wp_print_head_scripts', 5);
  add_action('wp_footer', 'wp_enqueue_scripts', 5);
}
add_action( 'wp_enqueue_scripts', 'move_scripts_head_to_footer_ex' );


/*	.svg 拡張子画像のアップロードを許可する
/*---------------------------------------------------------*/
function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');


/*	アイキャッチ設定でSVG画像のサムネイルを表示できるようにする
/*---------------------------------------------------------*/
function fix_svg_thumb_display() {
  echo '<style>
    td.media-icon img[src$=".svg"], img[src$=".svg"].attachment-post-thumbnail, #set-post-thumbnail img[src$=".svg"]{ 
    width: 100% !important; 
    height: auto !important; 
    }</style>';
}
add_action('admin_head', 'fix_svg_thumb_display');


/*	アイキャッチ画像を有効にする
/*---------------------------------------------------------*/
add_theme_support('post-thumbnails');


/*	editor-style.cssの追加
/*---------------------------------------------------------*/
function wpdocs_theme_add_editor_styles() {
  add_editor_style('editor-style.css');
}
add_action('admin_init', 'wpdocs_theme_add_editor_styles');


/*	デフォルトの投稿を消す（非表示にする）
/*---------------------------------------------------------*/
add_action('admin_menu', function () {
  remove_menu_page('edit.php');
});


/*	投稿記事のスラッグが日本語などマルチバイトの場合は、{投稿タイプ}-{記事ID}に強制的に変更
/*---------------------------------------------------------*/
function auto_post_slug( $slug, $post_ID, $post_status, $post_type ) {
  if ( preg_match( '/(%[0-9a-f]{2})+/', $slug ) ) {
    $slug = $post_ID;
  }
  return $slug;
}
add_filter( 'wp_unique_post_slug', 'auto_post_slug', 10, 4  );


/*	固定ページ化したカスタム投稿にはclassが付かないバグがある為、classを付与する
/*---------------------------------------------------------*/
function my_page_css_class( $css_class, $page ) {
  global $post;
  if ( $post->post_parent == $page->ID )
    $css_class[] = 'current_page_ancestor';
  if ( $post->ID == $page->ID ) {
    $css_class[] = 'current_page_item';
  }
  return $css_class;
}
add_filter( 'page_css_class', 'my_page_css_class', 10, 2 );


/*	投稿画面の「新規カテゴリーを追加」と「よく使うもの」を非表示にする
/*---------------------------------------------------------*/
function hide_category_tabs_adder() {
  global $pagenow;
  global $post_type;
  if (is_admin() && ($pagenow=='post-new.php' || $pagenow=='post.php')){
    echo '<style type="text/css">

/* カテゴリー */
#category-tabs, #category-adder {display:none;}

/* カスタムタクソノミー */
#blog_cat-tabs, #blog_cat-adder {display:none;}

.categorydiv .tabs-panel {padding: 0 !important; background: none; border: none !important;}
</style>';
  }
}
add_action( 'admin_head', 'hide_category_tabs_adder' );


/*	投稿画面のカスタムタクソノミーのチェックボックスをラジオボタンにする
/*---------------------------------------------------------*/
add_action( 'admin_print_footer_scripts', 'select_to_radio_custom_cat' );
function select_to_radio_custom_cat() {
?>
<script type="text/javascript">
  jQuery( function( $ ) {
    // 投稿画面
    $( '#taxonomy-blog_cat input[type=checkbox]' ).each( function() {
      $( this ).replaceWith( $( this ).clone().attr( 'type', 'radio' ) );
    } );

    // 一覧画面
    var blog_cat_checklist = $( '.blog_cat-checklist input[type=checkbox]' );
    blog_cat_checklist.click( function() {
      $( this blog_cat).parents( '.blog_cat-checklist' ).find( ' input[type=checkbox]' ).attr( 'checked', false );
      $( this ).attr( 'checked', true );
    } );
  } );
</script>
<?php
}

/*	投稿画面の見出しタグを制限する
/*---------------------------------------------------------*/
function custom_editor_settings($initArray){
  global $current_screen;
  switch ($current_screen->post_type){
    case 'page':
      $initArray['block_formats'] = 'Paragraph=p;Heading 3=h3;Heading 4=h4';
      break;
    case 'blog':
      $initArray['block_formats'] = 'Paragraph=p;Heading 2=h2;Heading 3=h3;Heading 4=h4;Heading 5=h5;Heading 6=h6';
      break;
  }
  return $initArray;
}
add_filter('tiny_mce_before_init', 'custom_editor_settings');


/*	【製品用カテゴリー】子カテゴリーにチェックを入れたら親カテゴリーにも自動でチェックが入るようにする
/*---------------------------------------------------------*/
function category_parent_check_script() {
?>
<script>
  jQuery(function($) {
    $('#taxonomy-blog_cat .children input').change(function() {
      function parentNodes(checked, nodes) {
        parents = nodes.parent().parent().parent().prev().children('input');
        if (parents.length != 0) {
          parents[0].checked = checked;
          parentNodes(checked, parents);
        }
      }
      var checked = $(this).is(':checked');
      $(this).parent().parent().siblings().children('label').children('input').each(function() {
        checked = checked || $(this).is(':checked');
      })
      parentNodes(checked, $(this));
    });
  });
</script>
<?php
}
add_action('admin_head-post-new.php', 'category_parent_check_script');
add_action('admin_head-post.php', 'category_parent_check_script');


/*	パンくずリスト
/*---------------------------------------------------------*/
function m_breadcrumb(){
  global $post;
  $str ='';
  if(!is_home()&&!is_admin()){ /* !is_admin は管理ページ以外という条件分岐 */
    $str.= '<ol itemscope itemtype="http://schema.org/BreadcrumbList">';
    $str.= '<li class="l-topicPath-arrow-home" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a href="' . home_url('/') .'" itemprop="item"><span itemprop="name">TOP</span></a>';
    $str.= '<meta itemprop="position" content="1" />';
    $str.= '</li>';


    /* お知らせ */
    // 一覧
    if(is_tax('blog_cat')){

      // 現在のページのカテゴリー・親カテゴリーの情報を取得し、変数化
      $get_meta = get_query_var('blog_cat');

      // 現在のページのカテゴリー
      $page_meta = get_term_by('slug',$get_meta,'blog_cat');
      $page_tax_id = $page_meta->term_id;                 // タームID
      $page_tax_slug = $page_meta->slug;                  // スラッグ
      $page_tax_name = $page_meta->name;                  // カテゴリー名

      // 現在のページの親カテゴリー
      $parent_meta = get_term( $page_meta->parent,'blog_cat' );
      $parent_id = $parent_meta->term_id;                 // タームID
      $parent_slug = $parent_meta->slug;                  // スラッグ
      $parent_name = $parent_meta->name;                  // カテゴリー名

      if(!empty($parent_id)){
        $str.='<li class="l-topicPath-arrow" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a href="'. home_url('/') .'custom/'.$parent_slug.'/'.'" itemprop="item"><span itemprop="name">'.$parent_name.'</span></a>';
        $str.= '<meta itemprop="position" content="2" />';
        $str.= '</li>';
        $str.= '<li class="l-topicPath-arrow" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="name">'.$page_tax_name.'</span>';
        $str.= '<meta itemprop="position" content="3" />';
        $str.= '</li>';
      }else{
        $str.= '<li class="l-topicPath-arrow" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="name">'.$page_tax_name.'</span>';
        $str.= '<meta itemprop="position" content="2" />';
        $str.= '</li>';
      }
    }
    // 詳細
    elseif(is_singular('blog')){
      $str.='<li class="l-topicPath-arrow" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a href="'. home_url('/') .'custom/shower/'.'" itemprop="item"><span itemprop="name">コインシャワー</span></a>';
      $str.= '<meta itemprop="position" content="2" />';
      $str.= '</li>';
      $str.= '<li class="l-topicPath-arrow" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="name">'.get_the_title().'</span>';
      $str.= '<meta itemprop="position" content="3" />';
      $str.= '</li>';
    }

    /* 固定ページ */
    elseif(is_page()){
      $cnt = 2;
      if($post -> post_parent != 0 ){
        $ancestors = array_reverse(get_post_ancestors( $post->ID ));
        foreach($ancestors as $ancestor){
          $str.='<li class="l-topicPath-arrow" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a href="'. get_permalink($ancestor).'" itemprop="item"><span itemprop="name">'. get_the_title($ancestor) .'</span></a>';
          $str.= '<meta itemprop="position" content="'.$cnt++.'" />';
          $str.= '</li>';
        }
      }
      $str.= '<li class="l-topicPath-arrow" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="name">'. $post -> post_title .'</span>';
      $str.= '<meta itemprop="position" content="'.$cnt++.'" />';
      $str.= '</li>';
    }

    /* 404 Not Found ページ */
    elseif(is_404()){
      $str.='<li class="l-topicPath-arrow" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="title">お探しのページは見つかりませんでした。</span>';
      $str.= '<meta itemprop="position" content="2" />';
      $str.= '</li>';
    }

    /* 検索結果ページ */
    elseif(is_search()){
      $str.='<li class="l-topicPath-arrow" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="name">検索結果：'.get_search_query().'</span>';
      $str.= '<meta itemprop="position" content="2" />';
      $str.= '</li>';
    } 

    /* その他のページ */
    else{
      $str.='<li class="l-topicPath-arrow" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="title">'. wp_title('', false) .'</span></li>';
    }
    $str.='</ol>';
  }
  echo $str;
}


/*	ページナビゲーション
/*---------------------------------------------------------*/
function m_page_navigation() {
  global $wp_rewrite;
  global $wp_query;
  global $paged;
  $paginate_base = get_pagenum_link(1);
  if(($wp_query->max_num_pages) > 1) {
    if (strpos($paginate_base, '?') || ! $wp_rewrite->using_permalinks()) {
      $paginate_format = '';
      $paginate_base = add_query_arg('paged', '%#%');
    } else {
      $paginate_format = (substr($paginate_base, -1 ,1) == '/' ? '' : '/') .
        user_trailingslashit('page/%#%/', 'paged');
      $paginate_base .= '%_%';
    }
    $result = paginate_links( array(
      'base' => $paginate_base,
      'format' => $paginate_format,
      'total' => $wp_query->max_num_pages,
      'mid_size' => 2,
      'current' => ($paged ? $paged : 1),
      'prev_text' => '<',
      'next_text' => '>',
    ));
    echo '<div class="m-pageNavi">'."\n".$result."</div>\n";
  }
}


/*	カスタマイザー
/*---------------------------------------------------------*/
// カスタマイザーのデフォルトを非表示
add_action('customize_register', 'themename_customize_register');
function themename_customize_register($wp_customize) {
  //    $wp_customize->remove_section( 'title_tagline' );
  //    $wp_customize->remove_section( 'nav' );
  $wp_customize->remove_section( 'static_front_page' );
}


/*	指定した投稿タイプのビジュアルエディターを非表示にする
/*---------------------------------------------------------*/
add_action('admin_print_styles', 'admin_parents_css_custom');
function admin_parents_css_custom() {
  global $typenow;
  if($typenow == 'page'){
    echo '<style>#postdivrich {display: none;}</style>';
  }
  elseif($typenow == 'custom'){
    echo '<style>#postdivrich {display: none;}</style>';
  }
}



/*	【ブログ】WordPress Popular Postsのカスタマイズ
/*---------------------------------------------------------*/
function my_custom_popular_html($mostpopular, $instance) {
  $fav_cnt = 1;
  foreach( $mostpopular as $popular ) {
?>
<?php $views = number_format(wpp_get_views($popular->id)); ?>
<?php // カスタム投稿『ブログ』の場合
    if($instance['post_type'] == 'blog'): ?>
<li>
  <a href="<?php echo get_the_permalink($popular->id); ?>">
    <div class="img js-imgliquid">
      <span><?php echo $fav_cnt; ?></span>
      <?php if(!empty(get_the_post_thumbnail( $popular->id, 'full' ))): ?>
      <img src="<?php echo get_the_post_thumbnail_url( $popular->id, 'full' ); ?>" alt="<?php the_title(); ?>">
      <?php else: ?>
      <?php endif; ?>
    </div>
    <div class="text">
      <p class="rank-title">
        <?php
          if(mb_strlen(get_the_title($popular->id), 'UTF-8') > 18){
            $title = mb_substr(get_the_title($popular->id), 0, 18, 'UTF-8');
            echo $title.'…';
          }else{
            echo get_the_title($popular->id);
          }
        ?>
      </p>
    </div>
  </a>
</li>
<?php endif; ?>
<?php
    $fav_cnt++;
  }
}
add_filter( 'wpp_custom_html', 'my_custom_popular_html', 10, 2 );


/*	【お問い合わせ】MW WP Form の自動返信メールに送信日時を設置
/*---------------------------------------------------------*/
function send_date_time( $value, $key, $insert_contact_data_id ) {
  if ( $key === 'send_datetime' ) {
    return date_i18n('Y/m/d');
  }
  return $value;
}
add_filter( 'mwform_custom_mail_tag_mw-wp-form-84', 'send_date_time', 10, 3 );


/*	【必要であれば追加する機能一覧】
/*---------------------------------------------------------*/

/*	【Custom Post Type Permalinks使用時】
/*---------------------------------------------------------
// パーマリンクの調整
function m_rewriterules($mypost, $myterm, $parents) {
    $newrules = array();
    if ( $myterm->parent == 0 ) {  //親タームがなかったら
        if (empty($parents)) {
            $newrules['archive'][$mypost . '/' . '(' . $myterm->slug . ')/?$'] = 'index.php?' . $myterm->taxonomy . '=$matches[1]';  //アーカイブページ用
            $newrules['archive'][$mypost . '/' . '(' . $myterm->slug . ')/page/([0-9]+)/?$'] = 'index.php?' . $myterm->taxonomy . '=$matches[1]&paged=$matches[2]';   //2ページ以降用
            $newrules['single'][$mypost . '/' . $myterm->slug . '/([^/]+)/?$'] = 'index.php?' . $mypost . '=$matches[1]';
        } else {  //親があるターム用
            $newrules['single'][$mypost . '/'. $myterm->slug . '/' . implode('/', $parents) . '/([^/]+)/?$'] = 'index.php?' . $mypost . '=$matches[1]';  //singleページ用
            $parents[count($parents)-1] = '(' . $parents[count($parents)-1] . ')/';
            $newrules['archive'][$mypost . '/' . $myterm->slug . '/' . implode('/', $parents) . '?$'] = 'index.php?' . $myterm->taxonomy . '=$matches[1]';  //アーカイブページ用
            $newrules['archive'][$mypost . '/' . $myterm->slug . '/' . implode('/', $parents) . 'page/([0-9]+)/?$'] = 'index.php?' . $myterm->taxonomy . '=$matches[1]&paged=$matches[2]';  //2ページ以降用
        }
    } else {  //親があったら
        $parentterm = get_term_by('id', $myterm->parent, $myterm->taxonomy);
        array_unshift($parents, $myterm->slug);
        $newrules = m_rewriterules($mypost, $parentterm, $parents);
    }
    return $newrules;
}

function m_rewriterules_option($rules) {  //引数：既に設定されているリライトルール
    $newrules = array();  $toriaezu = array();  $single = array();  $archive = array();
    $myposts = get_post_types(array('_builtin' => false));  //全カスタムタイプ取得②
    foreach($myposts as $mypost) {
        $mytaxonomies = get_taxonomies(array('object_type' => array($mypost))); //それのタクソノミー取得③
        foreach ($mytaxonomies as $mytaxonomie) {
            $myterms = get_terms( $mytaxonomie, array('hide_empty' => 0) ); //それのターム取得④
            foreach ($myterms as $myterm) {
                $toriaezu =  m_rewriterules($mypost, $myterm, array()); //リライトルール配列を生成
                $single += $toriaezu['single'];  //順番調整⑤
                $archive += $toriaezu['archive'];
            }
        }
    }
    return $archive + $single + $rules;  //新しいルールを追加して返す⑥
}
add_filter('rewrite_rules_array','m_rewriterules_option');
*/

?>