$(function(){
  $('.p-mv-box .img').delay(200).queue(function(){
    $(this).addClass('fadeIn');
  });
  $('.p-mv-box .description').delay(600).queue(function(){
    $(this).addClass('fadeIn');
  });
  
  $('.p-mv-box .orange').delay(1000).queue(function(){
    $(this).addClass('fadeIn');
  });
  
  var w = $(window).width();
  var x = 767;
  if (w <= x) {
    // ページ表示時のブラウザ横幅が767px以下の場合
    $('.l-nav .links ul li a').on('click', function(){
      $('.l-toggle').toggleClass('is-open');
      $('.l-nav').fadeToggle();
    });
  }
  var timer = false;
  $(window).resize(function() {
    if (timer !== false) {
      clearTimeout(timer);
    }
    timer = setTimeout(function() {
      var w = $(window).width();
      var x = 767;
      if (w <= x) {
        // 767px以下
        $('.l-nav .links ul li a').on('click', function(){
          $('.l-toggle').toggleClass('is-open');
          $('.l-nav').fadeToggle();
        });
      }else{
      }
    }, 0);
  });
});