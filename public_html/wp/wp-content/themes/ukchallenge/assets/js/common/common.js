/* ページ内リンクの処理  */
$(function(){

  var w = $(window).width();
  var x = 767;
  var headerHight = 100; // ヘッダーの高さ（PC）
  if (w <= x) {
    // ページ表示時のブラウザ横幅が767px以下の場合
    var headerHight = 70; // ヘッダーの高さ（SP）
  }
  var timer = false;
  $(window).resize(function() {
    if (timer !== false) {
      clearTimeout(timer);
    }
    timer = setTimeout(function() {
      var w = $(window).width();
      var x = 767;
      if (w <= x) {
        // 767px以下
        var headerHight = 70; // ヘッダーの高さ（SP）
      }else{
        // 767px以上
        var headerHight = 100; // ヘッダーの高さ（PC）
      }
    }, 0);
  });

  // #で始まるアンカーをクリックした場合に処理
  $('a[href^=#]').click(function() {
    // スクロールの速度
    var speed = 400; // ミリ秒
    // アンカーの値取得
    var href= $(this).attr("href");
    // 移動先を取得
    var target = $(href == "#" || href == "" ? 'html' : href);
    // 移動先を数値で取得
    var position = target.offset().top - headerHight;
    // スムーススクロール
    $('body,html').animate({scrollTop:position}, speed, 'swing');
    return false;
  });
});

/* ページ外リンクの処理  */
$(window).load(function (e) {

  var w = $(window).width();
  var x = 767;
  var headerHight = 100; // ヘッダーの高さ（PC）
  if (w <= x) {
    // ページ表示時のブラウザ横幅が767px以下の場合
    var headerHight = 70; // ヘッダーの高さ（SP）
  }
  var timer = false;
  $(window).resize(function() {
    if (timer !== false) {
      clearTimeout(timer);
    }
    timer = setTimeout(function() {
      var w = $(window).width();
      var x = 767;
      if (w <= x) {
        // 767px以下
        var headerHight = 70; // ヘッダーの高さ（SP）
      }else{
        // 767px以上
        var headerHight = 100; // ヘッダーの高さ（PC）
      }
    }, 0);
  });

  var hash = location.hash;	//アンカーリンク取得
  if($(hash).length){
    e.preventDefault();
    //IE判別
    var ua = window.navigator.userAgent.toLowerCase();
    var isIE = (ua.indexOf('msie') >= 0 || ua.indexOf('trident') >= 0);
    //IEだった場合
    if (isIE) {
      setTimeout(function(){
        var position = $(hash).offset().top;
        $("html, body").scrollTop(Number(position)-headerHight);
      },500);
      //IE以外
    } else {
      var position = $(hash).offset().top;	//アンカーリンクの位置取得
      $("html, body").scrollTop(Number(position)-headerHight);	//アンカーリンクの位置まで移動
    }
  }
});

/* imgLiquid.js */
$(function(){
  $(".js-imgliquid").imgLiquid();
});

/* ヘッダー・ナビ スクロール処理 */
$(function(){
  $('.l-header').clone().insertAfter('.l-header').addClass('is-clone').removeClass('home');
  $('.l-nav').clone().insertAfter('.l-nav').addClass('is-clone').removeClass('home');
  var $win = $(window);
  $win.on('load scroll', function() {
    var value = $(this).scrollTop();
    if ( value > 100 ) {
      $('.l-header.is-clone').addClass('is-scroll');
      $('.l-nav.is-clone').addClass('is-scroll');
      $('.m-sideFixed-box').addClass('is-scroll');
    } else {
      $('.l-header.is-clone').removeClass('is-scroll');
      $('.l-nav.is-clone').removeClass('is-scroll');
      $('.m-sideFixed-box').removeClass('is-scroll');
    }
  });
});

/* ナビゲーションメニューのトグル */
$(function() {
  // SP Toggle Button
  $('.l-toggle').on('click', function(){
    $(this).toggleClass('is-open');
    $('.l-nav').fadeToggle();
    return false;
  });
});

/* 表示中のページを判別し、class付与 */
$(function(){
  $('.l-nav .links ul li a').each(function(){
    var $href = $(this).prop('href');
    if ($href == location.href) {
      $(this).addClass('is-current');
    }
  });
});