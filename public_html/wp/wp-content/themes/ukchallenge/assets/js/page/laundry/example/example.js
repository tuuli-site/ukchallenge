/* タブ切替 */
$(function(){
  /*初期表示*/
  $('.p-box .tab-box01 .box').hide();
  $('.p-box .tab-box01 .box').eq(0).show();
  $('.p-box .tab-box01 .tab ul li').eq(0).addClass('active');
  $('.p-box .tab-box02 .box').hide();
  $('.p-box .tab-box02 .box').eq(0).show();
  $('.p-box .tab-box02 .tab ul li').eq(0).addClass('active');
  $('.p-box .tab-box03 .box').hide();
  $('.p-box .tab-box03 .box').eq(0).show();
  $('.p-box .tab-box03 .tab ul li').eq(0).addClass('active');
  $('.p-box .tab-box04 .box').hide();
  $('.p-box .tab-box04 .box').eq(0).show();
  $('.p-box .tab-box04 .tab ul li').eq(0).addClass('active');
  $('.p-box .tab-box05 .box').hide();
  $('.p-box .tab-box05 .box').eq(0).show();
  $('.p-box .tab-box05 .tab ul li').eq(0).addClass('active');
  $('.p-box .tab-box06 .box').hide();
  $('.p-box .tab-box06 .box').eq(0).show();
  $('.p-box .tab-box06 .tab ul li').eq(0).addClass('active');
  /*クリックイベント*/
  $('.p-box .tab-box01 .tab ul li').each(function () {
    $(this).on('click', function(){
      var index = $('.p-box .tab-box01 .tab ul li').index(this);
      $('.p-box .tab-box01 .tab ul li').removeClass('active');
      $(this).addClass('active');
      $('.p-box .tab-box01 .box').hide();
      $('.p-box .tab-box01 .box').eq(index).show();
    });
  });
  $('.p-box .tab-box02 .tab ul li').each(function () {
    $(this).on('click', function(){
      var index = $('.p-box .tab-box02 .tab ul li').index(this);
      $('.p-box .tab-box02 .tab ul li').removeClass('active');
      $(this).addClass('active');
      $('.p-box .tab-box02 .box').hide();
      $('.p-box .tab-box02 .box').eq(index).show();
    });
  });
  $('.p-box .tab-box03 .tab ul li').each(function () {
    $(this).on('click', function(){
      var index = $('.p-box .tab-box03 .tab ul li').index(this);
      $('.p-box .tab-box03 .tab ul li').removeClass('active');
      $(this).addClass('active');
      $('.p-box .tab-box03 .box').hide();
      $('.p-box .tab-box03 .box').eq(index).show();
    });
  });
  $('.p-box .tab-box04 .tab ul li').each(function () {
    $(this).on('click', function(){
      var index = $('.p-box .tab-box04 .tab ul li').index(this);
      $('.p-box .tab-box04 .tab ul li').removeClass('active');
      $(this).addClass('active');
      $('.p-box .tab-box04 .box').hide();
      $('.p-box .tab-box04 .box').eq(index).show();
    });
  });
  $('.p-box .tab-box05 .tab ul li').each(function () {
    $(this).on('click', function(){
      var index = $('.p-box .tab-box05 .tab ul li').index(this);
      $('.p-box .tab-box05 .tab ul li').removeClass('active');
      $(this).addClass('active');
      $('.p-box .tab-box05 .box').hide();
      $('.p-box .tab-box05 .box').eq(index).show();
    });
  });
  $('.p-box .tab-box06 .tab ul li').each(function () {
    $(this).on('click', function(){
      var index = $('.p-box .tab-box06 .tab ul li').index(this);
      $('.p-box .tab-box06 .tab ul li').removeClass('active');
      $(this).addClass('active');
      $('.p-box .tab-box06 .box').hide();
      $('.p-box .tab-box06 .box').eq(index).show();
    });
  });
});