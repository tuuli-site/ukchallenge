<?php get_header(); ?>
<div class="p-mv-box">
  <div class="logo">
    <a href="<?php echo home_url(); ?>">
      <img src="<?php echo get_theme_file_uri(); ?>/assets/images/common/header_img_logo.svg" alt="<?php bloginfo('home'); ?>">
    </a>
  </div>
  <div class="m-container">
    <div class="box">
      <div class="container">
        <h1>
          <img src="<?php echo get_theme_file_uri(); ?>/assets/images/top/mv_img_ttl.png" alt="世界を知り、可能性を広げる。">
        </h1>
        <p>
          <span>君が挑戦するのは、<br class="nopc">普通のイギリス留学じゃない</span>
        </p>
      </div>
    </div>
  </div>
  <div class="inquiry">
    <a href="#inquiry">
      <img src="<?php echo get_theme_file_uri(); ?>/assets/images/top/mv_img_inquiry.png" alt="無料相談受付中">
    </a>
  </div>
</div>

<div class="p-nav-box">
  <div class="m-container">
    <ul class="flex">
      <li>
        <a href="<?php echo home_url(); ?>" class="is-current">
          <span>HOME</span>
        </a>
      </li>
      <li>
        <a href="#about">
          <span>UK Challengeとは</span>
        </a>
      </li>
      <li>
        <a href="#genre">
          <span>留学ジャンル</span>
        </a>
      </li>
      <li>
        <a href="#blog">
          <span>ブログ</span>
        </a>
      </li>
      <li>
        <a href="#news">
          <span>お知らせ</span>
        </a>
      </li>
      <li>
        <a href="<?php echo home_url(); ?>/company/">
          <span>事業概要</span>
        </a>
      </li>
      <li>
        <a href="<?php echo home_url(); ?>/contact/">
          <span>お問い合わせ</span>
        </a>
      </li>
    </ul>
  </div>
</div>

<div class="p-about-box" id="about">
  <div class="m-container">
    <div class="m-title01">
      <div class="icon">
        <img src="<?php echo get_theme_file_uri(); ?>/assets/images/top/about_icn.svg" alt="UK Challengeとは アイコン">
      </div>
      <h2 class="title">UK Challengeとは</h2>
    </div>
    <p class="description">UK Challengeはサッカーやアートなど<br class="nopc">専門的な分野から一般的な語学留学を<br class="nopc">取り扱う<br class="nosp">イギリス専門の<br class="nopc">留学エージェントです。<br>サッカーやアートなど各分野に<br class="nopc">精通したスタッフが<br>現地でサポートをしてくれる提携先を<br class="nopc">ご紹介させていただきます。<br>一人一人のご希望に沿ったプランは<br class="nopc">もちろんですが、<br class="nosp">夢や目標実現の為の<br class="nopc">プランをご提案し皆さんの挑戦を<br class="nopc">全力で応援します。</p>
    <p class="description">サッカーチームの遠征、企業の<br class="nopc">研修旅行などもご希望に合わせて<br class="nopc">プランを作成させていただきます。<br>「英語力に自信が持てた」、<br class="nopc">「世界観が広がった」、<br class="nopc">「将来の夢ができた」、<br class="nopc">「海外でも戦える」、<br>帰国後に人生の可能性を広げる<br class="nopc">そんな言葉が出てくる留学を<br class="nopc">実現させます。</p>
  </div>
</div>

<div class="p-genre-box" id="genre">
  <div class="m-container">
    <div class="m-title01">
      <div class="icon">
        <img src="<?php echo get_theme_file_uri(); ?>/assets/images/top/genre_icn.svg" alt="留学ジャンル アイコン">
      </div>
      <h2 class="title">留学ジャンル</h2>
    </div>
    <ul class="flex">
      <li class="wow fadeInUp">
        <h3 data-mh="genre-heading">サッカー留学</h3>
        <p data-mh="genre-text">選手留学、指導者留学、トレーナー留学、サッカービジネス留学など様々な留学プランをご用意しています。</p>
        <div class="button">
          <a href="<?php echo home_url(); ?>/football/" class="m-button01 small">
            <span>詳しくはこちら</span>
          </a>
        </div>
      </li>
      <li class="wow fadeInUp" data-wow-delay=".1s">
        <h3 data-mh="genre-heading">サッカー遠征</h3>
        <p data-mh="genre-text">プロ下部組織との練習試合、プロコーチによる練習、プロ試合観戦、語学レッスン、指導者資格取得、スタジアムツアー、市内観光などご希望の内容でアレンジします。</p>
        <div class="button">
          <a href="<?php echo home_url(); ?>/football-trip/" class="m-button01 small">
            <span>詳しくはこちら</span>
          </a>
        </div>
      </li>
      <li class="wow fadeInUp" data-wow-delay=".2s">
        <h3 data-mh="genre-heading">アート・音楽留学</h3>
        <p data-mh="genre-text">アート留学ではデッサン、絵画、彫刻、グラフィック、デジタルなど、音楽留学ではボーカル、各楽器など様々なジャンルをご希望のレベルで学ぶことが可能です。</p>
        <div class="button">
          <a href="<?php echo home_url(); ?>/art-music/" class="m-button01 small">
            <span>詳しくはこちら</span>
          </a>
        </div>
      </li>
      <li class="wow fadeInUp">
        <h3 data-mh="genre-heading">語学留学</h3>
        <p data-mh="genre-text">質の高い語学学校をご紹介。ホームステイ手配も語学学校と協力して行うので安心して渡英していただけます。</p>
        <div class="button">
          <a href="<?php echo home_url(); ?>/learn-language/" class="m-button01 small">
            <span>詳しくはこちら</span>
          </a>
        </div>
      </li>
      <li class="wow fadeInUp" data-wow-delay=".1s">
        <h3 data-mh="genre-heading">企業研修・旅行</h3>
        <p data-mh="genre-text">各社に合わせた企業向け語学研修をアレンジします。現地企業のスタッフによる講義や各業界の現地サービス見学など柔軟にご相談いただけます。</p>
        <div class="button">
          <a href="<?php echo home_url(); ?>/intern-travel/" class="m-button01 small">
            <span>詳しくはこちら</span>
          </a>
        </div>
      </li>
      <li class="wow fadeInUp" data-wow-delay=".2s">
        <h3 data-mh="genre-heading">その他留学</h3>
        <p data-mh="genre-text">現地のボーディングスクールに通う正規留学やゴルフ、テニス、ラグビーなどイギリス発祥のスポーツ留学をご希望に合わせプランをアレンジします。</p>
        <div class="button">
          <a href="<?php echo home_url(); ?>/other/" class="m-button01 small">
            <span>詳しくはこちら</span>
          </a>
        </div>
      </li>
    </ul>
  </div>
</div>

<div class="p-blog-box" id="blog">
  <div class="m-container">
    <div class="m-title01">
      <div class="icon">
        <img src="<?php echo get_theme_file_uri(); ?>/assets/images/top/blog_icn.svg" alt="新着ブログ一覧 アイコン">
      </div>
      <h2 class="title">新着ブログ一覧</h2>
    </div>
    <ul class="flex wow fadeInUp">
      <?php
      $paged = get_query_var('paged')? get_query_var('paged') : 1;
      $args = array(
        'post_type' => 'blog',
        'posts_per_page' => 4,
        'paged' => $paged,
        'post_status' => 'publish'
      );
      ?>
      <?php $wp_query = new WP_Query( $args ); ?>
      <?php if( $wp_query->have_posts() ) : ?>
      <?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
      <li>
        <a href="<?php the_permalink(); ?>">
          <div class="img js-imgliquid">
            <?php if (has_post_thumbnail()): ?>
            <?php echo get_the_post_thumbnail($post->ID, 'full'); ?>
            <?php else: ?>
            <?php endif; ?>
            <?php
            $terms = get_the_terms($post -> ID, 'blog_cat');
            foreach($terms as $term): ?>
            <?php
            $cat_name = $term->name;
            ?>
            <?php endforeach; ?>
            <span class="cat"><?php echo $cat_name; ?></span>
          </div>
          <div class="text">
            <p class="day"><?php the_time('Y.m.d'); ?></p>
            <h3><?php the_title(); ?></h3>
            <p class="description">
              <?php
              $text_cnt = 62;
              $post_content = get_the_content();
              if(mb_strlen($post_content, 'UTF-8')>$text_cnt){
                $post_content_mb = mb_substr($post_content, 0, $text_cnt, 'UTF-8');
                echo $post_content_mb.'...';
              }else{
                echo $post_content;
              }
              ?>
            </p>
          </div>
        </a>
      </li>
      <?php endwhile; ?>
      <?php else: ?>
      <?php endif; ?>
      <?php wp_reset_query(); ?>
    </ul>
    <div class="button">
      <a href="<?php echo home_url(); ?>/blog/" class="m-button01">
        <span>詳しくはこちら</span>
      </a>
    </div>
  </div>
</div>

<div class="p-news-box" id="news">
  <div class="m-container">
    <div class="box">
      <div class="m-title01 small">
        <div class="icon">
          <img src="<?php echo get_theme_file_uri(); ?>/assets/images/top/news_icn.svg" alt="お知らせ アイコン">
        </div>
        <h2 class="title">お知らせ</h2>
      </div>
      <ul>
        <?php
        $paged = get_query_var('paged')? get_query_var('paged') : 1;
        $args = array(
        'post_type' => 'news',
        'posts_per_page' => 3,
        'paged' => $paged,
        'post_status' => 'publish'
        );
        ?>
        <?php $wp_query = new WP_Query( $args ); ?>
        <?php if( $wp_query->have_posts() ) : ?>
        <?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
        <li>
          <a href="<?php the_permalink(); ?>" class="flex">
            <div class="day">
              <p><?php the_time('Y.m.d'); ?></p>
            </div>
            <div class="title">
              <h3><?php the_title(); ?></h3>
            </div>
          </a>
        </li>
        <?php endwhile; ?>
        <?php else: ?>
        <?php endif; ?>
        <?php wp_reset_query(); ?>
      </ul>
      <div class="button">
        <a href="<?php echo home_url(); ?>/news/" class="m-button01">
          <span>一覧はこちら</span>
        </a>
      </div>
    </div>
  </div>
</div>

<div class="p-question-box">
  <div class="m-container">
    <div class="box">
      <div class="m-title01">
        <div class="icon">
          <img src="<?php echo get_theme_file_uri(); ?>/assets/images/top/question_icn.svg" alt="こんな不安はありませんか？ アイコン">
        </div>
        <h2 class="title">こんな不安はありませんか？</h2>
      </div>
      <ul class="flex wow fadeInUp">
        <li>
          <div class="img">
            <img src="<?php echo get_theme_file_uri(); ?>/assets/images/top/question_img_01.jpg" alt="イメージ">
          </div>
          <div class="text">
            <p>今の英語力で留学して<br>大丈夫なのかな？</p>
          </div>
        </li>
        <li>
          <div class="img">
            <img src="<?php echo get_theme_file_uri(); ?>/assets/images/top/question_img_02.jpg" alt="イメージ">
          </div>
          <div class="text">
            <p>留学費用ってどれくらい<br>用意しなきゃいけないのかな？</p>
          </div>
        </li>
        <li>
          <div class="img">
            <img src="<?php echo get_theme_file_uri(); ?>/assets/images/top/question_img_03.jpg" alt="イメージ">
          </div>
          <div class="text">
            <p>ビザ取得など留学準備って<br>大変なのかな？</p>
          </div>
        </li>
        <li>
          <div class="img">
            <img src="<?php echo get_theme_file_uri(); ?>/assets/images/top/question_img_04.jpg" alt="イメージ">
          </div>
          <div class="text">
            <p>留学後の進路や就職が<br>うまくいくか不安だな</p>
          </div>
        </li>
        <li>
          <div class="img">
            <img src="<?php echo get_theme_file_uri(); ?>/assets/images/top/question_img_05.jpg" alt="イメージ">
          </div>
          <div class="text">
            <p>環境や食事に馴染めるか<br>不安だな…</p>
          </div>
        </li>
      </ul>
    </div>
  </div>
</div>
<?php get_footer(); ?>