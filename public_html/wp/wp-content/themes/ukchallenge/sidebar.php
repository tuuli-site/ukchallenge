<div class="sidebar">
  <div class="category">
    <p class="heading">CATEGORY</p>
    <ul>
      <?php
      $catlist = wp_list_categories(array(
        'taxonomy' => 'blog_cat',
        'title_li' => '',
        'show_count' => 1,
        'echo' => 0
      ));
      $catlist = preg_replace('/<\/a> (\([0-9]*\))/', ' <span>$1</span></a>', $catlist);
      echo $catlist;
      ?>
    </ul>
  </div>
  <div class="ranking">
    <p class="heading">Ranking</p>
    <ul>
      <?php
      // 【プラグイン：WordPress Popular Posts】（custom-functions.phpにオプション記述あり）
      // 参考サイト：http://design.all-connect.jp/2692/
      if (function_exists('wpp_get_mostpopular')) {
        $args = array(
          // PV集計期間（daily, weekly, monthly, all から選べます）
          'range' => 'all',

          // PV数順で並び替え（comments を指定するとコメント順になります）
          'order_by' => 'views',

          // 投稿タイプ
          'post_type' => 'blog',

          // 表示数（single.php下部とのif文条件はこの数字）
          'limit' => 3,

          // 閲覧数表示するか（1なら表示、 0なら非表示）
          'stats_views' => '0',
        );
        wpp_get_mostpopular($args); //リストが出力される。
      }
      ?>
    </ul>
  </div>
</div>