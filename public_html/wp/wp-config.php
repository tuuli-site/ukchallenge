<?php
/**
 * WordPress の基本設定
 *
 * このファイルは、インストール時に wp-config.php 作成ウィザードが利用します。
 * ウィザードを介さずにこのファイルを "wp-config.php" という名前でコピーして
 * 直接編集して値を入力してもかまいません。
 *
 * このファイルは、以下の設定を含みます。
 *
 * * MySQL 設定
 * * 秘密鍵
 * * データベーステーブル接頭辞
 * * ABSPATH
 *
 * @link https://ja.wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// 注意:
// Windows の "メモ帳" でこのファイルを編集しないでください !
// 問題なく使えるテキストエディタ
// (http://wpdocs.osdn.jp/%E7%94%A8%E8%AA%9E%E9%9B%86#.E3.83.86.E3.82.AD.E3.82.B9.E3.83.88.E3.82.A8.E3.83.87.E3.82.A3.E3.82.BF 参照)
// を使用し、必ず UTF-8 の BOM なし (UTF-8N) で保存してください。

// ** MySQL 設定 - この情報はホスティング先から入手してください。 ** //
/** WordPress のためのデータベース名 */
define( 'DB_NAME', 'ohta_ukc' );

/** MySQL データベースのユーザー名 */
define( 'DB_USER', 'root' );

/** MySQL データベースのパスワード */
define( 'DB_PASSWORD', '' );

/** MySQL のホスト名 */
define( 'DB_HOST', 'localhost' );

/** データベースのテーブルを作成する際のデータベースの文字セット */
define( 'DB_CHARSET', 'utf8mb4' );

/** データベースの照合順序 (ほとんどの場合変更する必要はありません) */
define( 'DB_COLLATE', '' );

/**#@+
 * 認証用ユニークキー
 *
 * それぞれを異なるユニーク (一意) な文字列に変更してください。
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org の秘密鍵サービス} で自動生成することもできます。
 * 後でいつでも変更して、既存のすべての cookie を無効にできます。これにより、すべてのユーザーを強制的に再ログインさせることになります。
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'WphP%A~{/ljJPEn+`.Xqph2W0f7UB@Ns ,q&EWU|A=/mP~lSt(H[|,68e(BJ>qU>');
define('SECURE_AUTH_KEY',  'ujf|W8TNL:`-n?/i|YDFO&&uU|_Bo+|nt-nl`*qlt9k-aY84$A7PUY~{_E7=m2PW');
define('LOGGED_IN_KEY',    '$M1$M]%uX(9</(l-W8is)+7McUWiXQ[Rj^~iMYH92}&T |DJi$#H`&)9=zWoj% I');
define('NONCE_KEY',        '1[1Gx#IBB9i2BE6:SlXH+?sF.frSYh~|^v{&EBKTG@IDw(h206xA|k Wp$#z&-1g');
define('AUTH_SALT',        'N+[#w1%;MGCi4 |spwAxOh/cIjKL{~v-g(>jKulu}!6gYIc7y<C2%k[e|`8~0mX<');
define('SECURE_AUTH_SALT', '%F j2WatsPcH9vR>`{P!UspKz5U</y$?,rok-5I8YrFBY43>?-Q^v5+fZ,h+.Yiy');
define('LOGGED_IN_SALT',   'Ff&)M=HZ_(TqRrHa_%FSI+],F.ne:J(P}ZI;_9H[-r:fVH K9j.jkR:z[H*q#K-0');
define('NONCE_SALT',       'R.oPoLRv:1w_?onA=Fz|.x: rh+<yEN<a|m(z1kcSL[R/i-n*+4^)KkWzu-sUMW~');

/**#@-*/

/**
 * WordPress データベーステーブルの接頭辞
 *
 * それぞれにユニーク (一意) な接頭辞を与えることで一つのデータベースに複数の WordPress を
 * インストールすることができます。半角英数字と下線のみを使用してください。
 */
$table_prefix = 'wp_ukc_';

/**
 * 開発者へ: WordPress デバッグモード
 *
 * この値を true にすると、開発中に注意 (notice) を表示します。
 * テーマおよびプラグインの開発者には、その開発環境においてこの WP_DEBUG を使用することを強く推奨します。
 *
 * その他のデバッグに利用できる定数についてはドキュメンテーションをご覧ください。
 *
 * @link https://ja.wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* 編集が必要なのはここまでです ! WordPress でのパブリッシングをお楽しみください。 */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
